// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

pub mod account;
pub mod auth;
pub mod config;
pub mod file;
pub mod instance;
pub mod models;
pub mod path;

use auth::{azure, microsoft};
use instance::{fabric, running_instances::RunningInstances};
use std::sync::{Arc, Mutex};

#[cfg_attr(mobile, tauri::mobile_entry_point)]
pub fn run() {
    let client = reqwest::Client::new();
    let running_instances = Arc::new(Mutex::new(RunningInstances::new()));

    tauri::Builder::default()
        .plugin(tauri_plugin_process::init())
        .plugin(tauri_plugin_dialog::init())
        .plugin(tauri_plugin_shell::init())
        .invoke_handler(tauri::generate_handler![
            // Azure
            azure::microsoft_auth,
            // Microsoft
            microsoft::microsoft_login,
            microsoft::read_cached_minecraft_account,
            // Account
            account::upload_skin,
            account::reset_skin,
            account::set_cape,
            account::clear_capes,
            account::check_username_status,
            account::sign_out,
            // Instance
            instance::retrieve_versions,
            instance::get_installed_instances,
            instance::remove_installed_instance,
            instance::write_instance_file,
            instance::download_minecraft_instance,
            instance::update_minecraft_instance,
            instance::download_java,
            instance::run_instance,
            instance::delete_instance,
            instance::stop_instance,
            instance::modrinth::modrinth_search_mods,
            instance::modrinth::get_modrinth_add_ons,
            instance::modrinth::get_modrinth_mod,
            instance::modrinth::get_modrinth_mod_versions,
            instance::modrinth::get_modrinth_mod_version,
            instance::modrinth::get_latest_modrinth_mod_version,
            instance::modrinth::get_mod_upgrade_downgrade_status,
            instance::modrinth::update_mod,
            // Fabric
            fabric::get_supported_fabric_versions,
            // Config
            config::read_global_config,
            config::write_global_config,
        ])
        .manage(client)
        .manage(running_instances)
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
