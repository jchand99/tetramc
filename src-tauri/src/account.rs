use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use tauri::Manager;

use crate::path;

// use crate::logging::LogDispatcher;

#[derive(Serialize, Deserialize)]
pub struct Skin {
    id: String,
    state: String,
    url: String,
    variant: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Cape {
    id: String,
    state: String,
    url: String,
    alias: String,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CapePayload {
    cape_id: String,
}

#[derive(Serialize, Deserialize)]
pub struct ProfileAction {}

#[derive(Serialize, Deserialize)]
pub struct AccountUpdateResponse {
    id: String,
    name: String,
    skins: Vec<Skin>,
    capes: Vec<Cape>,
    profile_actions: Option<ProfileAction>,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct AccountUpdateErrorResponse {
    path: String,
    error_message: String,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all(deserialize = "camelCase"))]
#[serde(rename_all(serialize = "snake_case"))]
pub struct UsernameStatusResponse {
    changed_at: String,
    created_at: String,
    name_change_allowed: bool,
}

#[derive(Serialize, Deserialize)]
pub struct NameAvailabilityResponse {
    status: String,
}

#[tauri::command]
pub async fn change_username(
    client: tauri::State<'_, reqwest::Client>,
    // logger: tauri::State<'_, Arc<LogDispatcher>>,
    access_token: String,
    username: String,
) -> Result<AccountUpdateResponse, String> {
    let availability =
        check_username_availability(&client, /*&logger,*/ &access_token, &username).await?;

    match availability.status.as_str() {
        "AVAILABLE" => {
            match client
                .put(format!(
                    "https://api.minecraftservices.com/minecraft/profile/name/{}",
                    username
                ))
                .bearer_auth(access_token)
                .send()
                .await
                .map_err(|e| e.to_string())?
                .json::<AccountUpdateResponse>()
                .await
            {
                Ok(response) => Ok(response),
                Err(err) => {
                    // logger.err(err.to_string());
                    Err(err.to_string())
                }
            }
        }
        "NOT_ALLOWED" => Err(format!("'{}' is not allowed.", username)),
        "DUPLICATE" => {
            // logger.err("Username is not allowed or a duplicate!");
            Err("Someone already has this username.".to_string())
        }
        _ => {
            // logger.err("Invalid username update response.");
            Err("Invalid username update response.".to_string())
        }
    }
}

async fn check_username_availability(
    client: &reqwest::Client,
    // logger: &Arc<LogDispatcher>,
    access_token: &str,
    username: &str,
) -> Result<NameAvailabilityResponse, String> {
    match client
        .get(format!(
            "https://api.minecraftservices.com/minecraft/profile/name/{}/available",
            username
        ))
        .bearer_auth(access_token)
        .send()
        .await
        .map_err(|e| e.to_string())?
        .json::<NameAvailabilityResponse>()
        .await
    {
        Ok(response) => Ok(response),
        Err(err) => {
            // logger.err(err.to_string());
            Err(format!("Failed to update username: {}", err.to_string()))
        }
    }
}

#[tauri::command]
pub async fn check_username_status(
    client: tauri::State<'_, reqwest::Client>,
    // logger: tauri::State<'_, Arc<LogDispatcher>>,
    access_token: String,
) -> Result<UsernameStatusResponse, String> {
    match client
        .get("https://api.minecraftservices.com/minecraft/profile/namechange")
        .bearer_auth(access_token)
        .send()
        .await
        .map_err(|e| e.to_string())?
        .json::<UsernameStatusResponse>()
        .await
    {
        Ok(response) => Ok(response),
        Err(err) => {
            // logger.err(err.to_string());
            Err(err.to_string())
        }
    }
}

#[tauri::command]
pub async fn clear_capes(
    client: tauri::State<'_, reqwest::Client>,
    access_token: String,
) -> Result<(), String> {
    client
        .delete("https://api.minecraftservices.com/minecraft/profile/capes/active")
        .bearer_auth(access_token)
        .send()
        .await
        .map_err(|e| e.to_string())?;
    Ok(())
}

#[tauri::command]
pub async fn set_cape(
    client: tauri::State<'_, reqwest::Client>,
    // logger: tauri::State<'_, Arc<LogDispatcher>>,
    cape: Cape,
    access_token: String,
) -> Result<AccountUpdateResponse, String> {
    match client
        .put("https://api.minecraftservices.com/minecraft/profile/capes/active")
        .bearer_auth(access_token)
        .json(&CapePayload { cape_id: cape.id })
        .send()
        .await
        .map_err(|e| e.to_string())?
        .json::<AccountUpdateResponse>()
        .await
    {
        Ok(response) => Ok(response),
        Err(err) => {
            // logger.err(err.to_string());
            Err(err.to_string())
        }
    }
}

#[tauri::command]
pub async fn reset_skin(
    client: tauri::State<'_, reqwest::Client>,
    // logger: tauri::State<'_, Arc<LogDispatcher>>,
    access_token: String,
) -> Result<AccountUpdateResponse, String> {
    match client
        .delete("https://api.minecraftservices.com/minecraft/profile/skins/active")
        .bearer_auth(access_token)
        .send()
        .await
        .map_err(|e| e.to_string())?
        .json::<AccountUpdateResponse>()
        .await
    {
        Ok(response) => Ok(response),
        Err(err) => {
            // logger.err(err.to_string());
            Err(err.to_string())
        }
    }
}

#[tauri::command]
pub async fn upload_skin(
    client: tauri::State<'_, reqwest::Client>,
    // logger: tauri::State<'_, Arc<LogDispatcher>>,
    skin: String,
    variant: String,
    access_token: String,
) -> Result<AccountUpdateResponse, String> {
    if let Ok(bytes) = std::fs::read(&skin) {
        let part = reqwest::multipart::Part::bytes(bytes)
            .file_name(skin)
            .mime_str("image/png")
            .unwrap();
        let form = reqwest::multipart::Form::new()
            .text("variant", variant)
            .part("file", part);

        let res = client
            .post("https://api.minecraftservices.com/minecraft/profile/skins")
            .bearer_auth(access_token)
            .multipart(form)
            .send()
            .await
            .map_err(|e| e.to_string())?;

        match res.status() {
            StatusCode::OK => Ok(res
                .json::<AccountUpdateResponse>()
                .await
                .map_err(|e| e.to_string())?),
            _ => {
                let json = res
                    .json::<AccountUpdateErrorResponse>()
                    .await
                    .map_err(|e| e.to_string())?;

                Err(json.error_message)
            }
        }
    } else {
        // logger.err(format!("Failed to read skin file: {}", &skin));
        Err(format!("Failed to read skin file: {}", &skin))
    }
}

// TODO: Create tetramc path if it doesn't exist
pub const MINECRAFT_ACCOUNT_CACHE_FILE: &'static str = "minecraft_account.toml";
#[tauri::command]
pub fn sign_out(
    app: tauri::AppHandle, /*logger: tauri::State<'_, Arc<LogDispatcher>>*/
) -> Result<(), String> {
    let mut cache_dir = path::tetramc_cache_dir(&app)?;
    cache_dir.push(MINECRAFT_ACCOUNT_CACHE_FILE);

    std::fs::remove_file(&cache_dir).map_err(|_| "Unable to remove account cache file.")?;

    Ok(())
}
