use serde::{Deserialize, Serialize};

#[derive(Serialize, Clone, Deserialize)]
pub struct DownloadStatusPayload {
    pub message: String,
    pub percent: f64,
}
