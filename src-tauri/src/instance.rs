pub mod fabric;
pub(crate) mod java;
mod management;
pub mod modrinth;
pub mod running_instances;
mod vanilla;
use std::sync::{Arc, Mutex};

use chrono::Utc;
use modrinth::models::ModrinthProjectAction;
use tauri::Emitter;
use tauri_plugin_shell::ShellExt;
use vanilla::models::MinecraftVersionResponse;

use crate::{
    auth::microsoft::MinecraftAccount,
    config::GlobalConfig,
    path::{tetramc_instance_dir, tetramc_instance_subdir},
};

use self::{
    running_instances::RunningInstances,
    vanilla::models::{TetraMCInstance, TetraMCInstanceType},
};

const VANILLA_MINECRAFT_VERSIONS: &'static str =
    "https://launchermeta.mojang.com/mc/game/version_manifest.json";

#[tauri::command]
pub async fn retrieve_versions(
    client: tauri::State<'_, reqwest::Client>,
) -> Result<MinecraftVersionResponse, String> {
    Ok(client
        .get(VANILLA_MINECRAFT_VERSIONS)
        .send()
        .await
        .map_err(|e| e.to_string())?
        .json::<MinecraftVersionResponse>()
        .await
        .map_err(|e| format!("Failed to retrieve minecraft versions: {:?}", e))?)
}

#[tauri::command]
pub async fn update_minecraft_instance(
    app: tauri::AppHandle,
    client: tauri::State<'_, reqwest::Client>,
    mut edit_instance: TetraMCInstance,
    url: String,
    version_id: String,
    mod_actions: Vec<ModrinthProjectAction>,
    instance_name: String,
    ty: TetraMCInstanceType,
) -> Result<TetraMCInstance, String> {
    // If nothing changed, don't update anything
    if mod_actions.is_empty()
        && instance_name == edit_instance.name
        && version_id == edit_instance.id
        && ty == edit_instance.ty
    {
        println!("Nothing changed!");
        return Ok(edit_instance);
    }

    println!("Something changed, updating instance!");

    if instance_name != edit_instance.name {
        let instance_path = tetramc_instance_dir(&app, &edit_instance.name)?;
        let new_instance_path = tetramc_instance_dir(&app, &instance_name)?;

        std::fs::rename(instance_path, new_instance_path)
            .map_err(|e| format!("Failed to re-name instance: {:?}", e))?;

        edit_instance.name = instance_name.clone();
    }

    // FIXME: Hacky way to change the instance mc version and libraries;
    let mut modified_instance =
        management::download_instance(&app, &client, url, &instance_name, ty, &mod_actions).await?;

    match edit_instance.ty {
        TetraMCInstanceType::Fabric => {
            let edit_instance_mods = edit_instance.mods.get_or_insert_default();

            // Delete mods if there are any to delete.
            // HINT: Need to use edit_instance_mods because modified_instance returns with an empty instance array.
            let mut mods_path =
                tetramc_instance_subdir(&app, &modified_instance.name, ".minecraft")?;
            mods_path.push("mods");

            println!("Modified Instance Mods: {:?}", &edit_instance_mods);
            // Get the indexes indo the map for the mods that need to be removed.
            let removed_mods = mod_actions
                .iter()
                .filter_map(|action| match action {
                    ModrinthProjectAction::Delete { project_id, .. } => Some(project_id),
                    ModrinthProjectAction::Upgrade { project_id, .. } => Some(project_id),
                    ModrinthProjectAction::Downgrade { project_id, .. } => Some(project_id),
                    _ => None,
                })
                .flat_map(|id| {
                    if let Some(index) = edit_instance_mods
                        .iter()
                        .position(|i| i.project_id.as_str() == id)
                    {
                        println!("Found mod to delete! {:?}", index);
                        Some(edit_instance_mods.remove(index))
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>();

            println!(
                "Mods that need to be deleted from disk: {:?}",
                &removed_mods
            );
            for removed_mod in &removed_mods {
                if let Some(file_name) = removed_mod.file_name.as_ref() {
                    mods_path.push(&file_name);

                    println!("Deleting mod file: {:?}", &mods_path);
                    if mods_path.exists() {
                        std::fs::remove_file(&mods_path)
                            .map_err(|e| format!("Failed to delete mod: {:?}", e))?;
                    }
                    mods_path.pop();
                }
            }

            let mods_to_install = mod_actions
                .iter()
                .filter_map(|action| match action {
                    ModrinthProjectAction::Install {
                        project_id,
                        version_id,
                        ..
                    } => Some((project_id.as_ref(), version_id.as_ref())),
                    ModrinthProjectAction::Upgrade {
                        project_id,
                        to_version_id,
                        ..
                    } => Some((project_id.as_ref(), to_version_id.as_ref())),
                    ModrinthProjectAction::Downgrade {
                        project_id,
                        to_version_id,
                        ..
                    } => Some((project_id.as_ref(), to_version_id.as_ref())),
                    _ => None,
                })
                .collect::<Vec<_>>();

            let mut downloaded_mods =
                modrinth::download_mods(&app, &client, &instance_name, &mods_to_install).await?;

            edit_instance_mods.append(&mut downloaded_mods);

            modified_instance.mods = edit_instance.mods;
        }
        _ => {}
    }

    // Re-write the updated instance toml file.
    management::write_instance_toml(&app, &modified_instance)?;
    Ok(modified_instance)
}

#[tauri::command]
pub async fn download_minecraft_instance(
    app: tauri::AppHandle,
    client: tauri::State<'_, reqwest::Client>,
    url: String,
    instance_name: String,
    ty: TetraMCInstanceType,
    mod_actions: Vec<ModrinthProjectAction>,
) -> Result<TetraMCInstance, String> {
    let mut instance =
        management::download_instance(&app, &client, url, instance_name, ty, &mod_actions).await?;

    if !mod_actions.is_empty() {
        println!("{:?}", &mod_actions);
        let mods_to_install = mod_actions
            .iter()
            .filter_map(|action| match action {
                ModrinthProjectAction::Install {
                    project_id,
                    version_id,
                    ..
                } => Some((project_id.as_ref(), version_id.as_ref())),
                _ => None,
            })
            .collect::<Vec<_>>();

        let mods = modrinth::download_mods(&app, &client, &instance.name, &mods_to_install).await?;

        instance.mods = Some(mods);

        management::write_instance_toml(&app, &instance)?;
    }

    Ok(instance)
}

#[tauri::command]
pub async fn download_java(
    app: tauri::AppHandle,
    client: tauri::State<'_, reqwest::Client>,
) -> Result<(), String> {
    java::download(&app, &client).await
}

#[tauri::command]
pub async fn write_instance_file(
    app: tauri::AppHandle,
    instance: TetraMCInstance,
) -> Result<(), String> {
    management::write_instance_toml(&app, &instance)
}

#[tauri::command]
pub async fn get_installed_instances(
    app: tauri::AppHandle,
) -> Result<Vec<TetraMCInstance>, String> {
    management::get_instances(&app)
}

#[tauri::command]
pub fn remove_installed_instance(
    app: tauri::AppHandle,
    instance_name: String,
) -> Result<(), String> {
    management::remove_instance(&app, instance_name)
}

#[tauri::command]
pub fn stop_instance(
    running_instances: tauri::State<'_, Arc<Mutex<RunningInstances>>>,
    instance: TetraMCInstance,
) -> Result<(), String> {
    if let Ok(mut running_instances) = running_instances.lock() {
        Ok(running_instances.kill(instance.name)?)
    } else {
        Err("Failed to grab running instances!".to_string())
    }
}

#[tauri::command]
pub fn delete_instance(app: tauri::AppHandle, instance: TetraMCInstance) -> Result<(), String> {
    let instance_path = tetramc_instance_dir(&app, instance.name)?;

    std::fs::remove_dir_all(&instance_path)
        .map_err(|e| format!("Failed to delete instance: {:?}", e))?;

    Ok(())
}

#[tauri::command]
pub async fn run_instance(
    app: tauri::AppHandle,
    running_instances: tauri::State<'_, Arc<Mutex<RunningInstances>>>,
    global_config: GlobalConfig,
    mut instance: TetraMCInstance,
    account: MinecraftAccount,
) -> Result<(), String> {
    let dot_minecraft = tetramc_instance_subdir(&app, &instance.name, ".minecraft")?;
    if !dot_minecraft.exists() {
        std::fs::create_dir(&dot_minecraft)
            .map_err(|e| format!("Failed to create .minecraft directory: {:?}", e))?;
    }

    let args = crate::instance::management::get_game_arguments(
        &app,
        &instance,
        &global_config,
        &account,
        dot_minecraft.clone(),
    )?;

    let shell = app.shell();

    let command = if let Some(java_path) = &instance.java_path {
        shell.command(java_path)
    } else {
        shell.command(global_config.java_path)
    };

    let command = command
        .current_dir(&dot_minecraft)
        .args(args.split(crate::instance::management::DELIMITER));

    println!("Command: {:#?}", &command);

    instance.last_played = Some(Utc::now());
    management::write_instance_toml(&app, &instance)?;

    match command.spawn() {
        Ok((mut rx, child)) => {
            if let Ok(mut running_instances) = running_instances.lock() {
                running_instances.insert(instance.name.clone(), child);
            }

            let instances_clone = Arc::clone(&running_instances);
            let name_clone = instance.name.clone();

            // FIXME: Find some way to grab an error if this fails to spawn!
            tauri::async_runtime::spawn(async move {
                while let Some(event) = rx.recv().await {
                    match event {
                        tauri_plugin_shell::process::CommandEvent::Stderr(payload) => {
                            println!("StdErr: {}", std::str::from_utf8(&payload).unwrap());
                        }
                        tauri_plugin_shell::process::CommandEvent::Stdout(payload) => {
                            // println!("StdOut: {}", std::str::from_utf8(&payload).unwrap());
                        }
                        tauri_plugin_shell::process::CommandEvent::Error(payload) => {
                            println!("Error: {}", payload);
                        }
                        tauri_plugin_shell::process::CommandEvent::Terminated(_payload) => {
                            println!("Game terminated: {:?}", _payload);
                            if let Ok(mut instances_clone) = instances_clone.lock() {
                                instances_clone.manually_remove_terminated_child(&name_clone);
                            }

                            app.emit("child-process-terminated", &name_clone).unwrap();

                            println!("{:?}", instances_clone);
                            break;
                        }
                        _ => {}
                    }
                }
            });
        }
        Err(e) => {
            println!("FAILURE: {:?}", e);
            return Err(format!("Failed to start minecraft: {:?}", e));
        }
    }

    Ok(())
}
