use serde::{Deserialize, Serialize};

use crate::account::MINECRAFT_ACCOUNT_CACHE_FILE;

use super::{
    azure::{self, AzureAuthResponse, AzureTokenResponse},
    minecraft::{self, MinecraftAuthResponse, MinecraftProfile},
    xbox,
};

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftAccount {
    pub profile: MinecraftProfile,
    pub azure_auth_response: AzureAuthResponse,
    pub azure_token: AzureTokenResponse,
    pub auth: MinecraftAuthResponse,
}

#[tauri::command]
pub async fn microsoft_login(
    app: tauri::AppHandle,
    // logger: tauri::State<'_, Arc<LogDispatcher>>,
    client: tauri::State<'_, reqwest::Client>,
    azure_auth_response: AzureAuthResponse,
    azure_token_response: Option<AzureTokenResponse>,
) -> Result<MinecraftAccount, String> {
    let azure_token = if let Some(azure_token_response) = azure_token_response {
        // logger.info("Refreshing account information");
        azure::refresh_azure_token(&client, &azure_auth_response, &azure_token_response)
            .await
            .map_err(|e| {
                format!(
                    "Unable to refresh authentication. Please log in again. {}",
                    e
                )
            })
    } else {
        // logger.info("Signing in with login credentials");
        azure::azure_token(&client, &azure_auth_response)
            .await
            .map_err(|e| {
                format!(
                    "Unable to authenticate. Please try auth process again. {}",
                    e
                )
            })
    };

    let azure_token = match azure_token {
        Ok(token) => token,
        Err(e) => return Err(e),
    };

    let xbox_xbs_token = match xbox::xbox_xbs_auth(&client, &azure_token).await {
        Ok(token) => token,
        Err(_) => {
            return Err(String::from(
                "Unable to authenticate with xbs service. Please try again.",
            ))
        }
    };

    let xbox_xsts_token = match xbox::xbox_xsts_token(&client, xbox_xbs_token).await {
        Ok(token) => token,
        Err(_) => {
            return Err(String::from(
                "Unable to authenticate with xsts service. Please try again.",
            ))
        }
    };

    let minecraft_auth = minecraft::minecraft_xbox_auth(&client, xbox_xsts_token).await?;

    if !minecraft::owns_minecraft(&client, &minecraft_auth).await? {
        return Err("The logged in account does not own Minecraft.".into());
    }

    let minecraft_profile = minecraft::minecraft_profile(&client, &minecraft_auth).await?;

    let minecraft_account = MinecraftAccount {
        profile: minecraft_profile,
        azure_token,
        auth: minecraft_auth,
        azure_auth_response,
    };

    // If this fails they just log back in.
    if let Ok(contents) = toml::to_string(&minecraft_account) {
        crate::file::write_cache_file(&app, contents, MINECRAFT_ACCOUNT_CACHE_FILE)?;
    }

    Ok(minecraft_account)
}

#[tauri::command]
pub fn read_cached_minecraft_account(
    app: tauri::AppHandle,
) -> Result<Option<MinecraftAccount>, String> {
    match crate::file::read_cache_file(&app, MINECRAFT_ACCOUNT_CACHE_FILE) {
        Ok(content) => match toml::from_str::<MinecraftAccount>(&content) {
            Ok(account) => Ok(Some(account)),
            Err(_) => Ok(None),
        },
        Err(_) => Ok(None),
    }
}
