use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct AzureAuthResponse {
    pub user_code: String,
    pub device_code: String,
    pub verification_uri: String,
    pub expires_in: i64,
    pub interval: i64,
    pub message: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AzureTokenResponse {
    pub token_type: String,
    pub scope: String,
    pub expires_in: i64,
    pub ext_expires_in: i64,
    pub access_token: String,
    pub refresh_token: String,
}

#[tauri::command]
pub async fn microsoft_auth(
    client: tauri::State<'_, reqwest::Client>,
) -> Result<AzureAuthResponse, String> {
    println!("Authenticating with Microsoft");
    match client
        .get("https://login.microsoftonline.com/consumers/oauth2/v2.0/devicecode")
        .form(&[
            ("client_id", super::AZURE_CLIENT_ID),
            ("scope", "xboxlive.signin offline_access"),
        ])
        .send()
        .await
        .map_err(|e| e.to_string())?
        .json::<AzureAuthResponse>()
        .await
    {
        Ok(response) => {
            return Ok(response);
        }
        Err(err) => {
            return Err(format!(
                "Unable to authenticate. Please try clicking the url and entering the code again. {:?}",
                    err));
        }
    }
}

pub async fn azure_token(
    client: &reqwest::Client,
    azure_auth_response: &AzureAuthResponse,
) -> Result<AzureTokenResponse, String> {
    println!("Getting azure token");
    match client
        .post("https://login.microsoftonline.com/consumers/oauth2/v2.0/token")
        .form(&[
            ("client_id", super::AZURE_CLIENT_ID),
            ("grant_type", "urn:ietf:params:oauth:grant-type:device_code"),
            ("device_code", &azure_auth_response.device_code),
        ])
        .send()
        .await
        .map_err(|e| e.to_string())?
        .json::<AzureTokenResponse>()
        .await
    {
        Ok(response) => return Ok(response),
        Err(err) => return Err(err.to_string()),
    }
}

pub async fn refresh_azure_token(
    client: &reqwest::Client,
    azure_auth_response: &AzureAuthResponse,
    azure_token_response: &AzureTokenResponse,
) -> Result<AzureTokenResponse, String> {
    println!("Refreshing azure token!");
    match client
        .post("https://login.microsoftonline.com/consumers/oauth2/v2.0/token")
        .form(&[
            ("client_id", super::AZURE_CLIENT_ID),
            ("grant_type", "refresh_token"),
            ("device_code", &azure_auth_response.device_code),
            ("refresh_token", &azure_token_response.refresh_token),
        ])
        .send()
        .await
        .map_err(|e| e.to_string())?
        .json::<AzureTokenResponse>()
        .await
    {
        Ok(response) => return Ok(response),
        Err(err) => return Err(err.to_string()),
    };
}
