use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use serde_json::json;

use super::azure::AzureTokenResponse;

#[derive(Debug, Serialize, Deserialize)]
pub struct XboxUserHash {
    pub uhs: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct XboxDisplayClaims {
    pub xui: Vec<XboxUserHash>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct XboxXBSTokenResponse {
    pub issue_instant: String,
    pub not_after: String,
    pub token: String,
    pub display_claims: XboxDisplayClaims,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct XboxXSTSTokenResponse {
    pub issue_instant: String,
    pub not_after: String,
    pub token: String,
    pub display_claims: XboxDisplayClaims,
}

pub async fn xbox_xbs_auth(
    client: &reqwest::Client,
    azure_token: &AzureTokenResponse,
) -> Result<XboxXBSTokenResponse, String> {
    let body = json!({
        "Properties": {
            "AuthMethod": "RPS",
            "SiteName": "user.auth.xboxlive.com",
            "RpsTicket": &format!("d={}", &azure_token.access_token)
        },
        "RelyingParty": "http://auth.xboxlive.com",
        "TokenType": "JWT"
    });

    let res = client
        .post("https://user.auth.xboxlive.com/user/authenticate")
        .header("Content-Type", "application/json")
        .header("Accept", "application/json")
        .json(&body)
        .send()
        .await;

    if let Ok(response) = res {
        match response.status() {
            StatusCode::OK => match response.json::<XboxXBSTokenResponse>().await {
                Ok(response) => Ok(response),
                Err(err) => Err(err.to_string()),
            },
            _ => Err("Failed to get Xbox XBS token!".into()),
        }
    } else {
        Err("Failed to get Xbox XBS token!".into())
    }
}

pub async fn xbox_xsts_token(
    client: &reqwest::Client,
    xbox_xbs_token: XboxXBSTokenResponse,
) -> Result<XboxXSTSTokenResponse, String> {
    let body = json!({
        "Properties": {
            "SandboxId": "RETAIL",
            "UserTokens": [
                &xbox_xbs_token.token
            ]
        },
        "RelyingParty": "rp://api.minecraftservices.com/",
        "TokenType": "JWT",
    });

    let res = client
        .post("https://xsts.auth.xboxlive.com/xsts/authorize")
        .json(&body)
        .send()
        .await;

    if let Ok(response) = res {
        match response.status() {
            StatusCode::OK => match response.json::<XboxXSTSTokenResponse>().await {
                Ok(response) => Ok(response),
                Err(err) => Err(err.to_string()),
            },
            _ => Err("Failed to get Xbox XSTS token!".into()),
        }
    } else {
        Err("Failed to get Xbox XSTS token!".into())
    }
}
