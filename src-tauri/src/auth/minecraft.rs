use super::xbox::XboxXSTSTokenResponse;
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use serde_json::json;

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftAuthMetadata;

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftAuthResponse {
    pub username: String,
    pub roles: Vec<String>,
    //metadata: MinecraftAuthMetadata,
    pub access_token: String,
    pub expires_in: i64,
    pub token_type: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftItem {
    name: String,
    signature: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftOwnershipResponse {
    items: Vec<MinecraftItem>,
    signature: String,
    #[serde(alias = "keyId")]
    key_id: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftSkin {
    id: String,
    state: String,
    url: String,
    variant: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftCape {
    id: String,
    state: String,
    url: String,
    alias: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftProfile {
    pub id: String,
    pub name: String,
    pub skins: Vec<MinecraftSkin>,
    pub capes: Vec<MinecraftCape>,
}

pub async fn minecraft_xbox_auth(
    client: &reqwest::Client,
    xbox_xsts_token: XboxXSTSTokenResponse,
) -> Result<MinecraftAuthResponse, String> {
    let body = json!({
        "identityToken": &format!("XBL3.0 x={};{}", &xbox_xsts_token.display_claims.xui[0].uhs, &xbox_xsts_token.token),
    });

    let res = client
        .post("https://api.minecraftservices.com/authentication/login_with_xbox")
        .json(&body)
        .send()
        .await;

    if let Ok(response) = res {
        match response.status() {
            StatusCode::OK => match response.json::<MinecraftAuthResponse>().await {
                Ok(response) => Ok(response),
                Err(err) => Err(err.to_string()),
            },
            _ => Err(format!(
                "Failed to authenticate with minecraft using Xbox credentials: {:?}",
                response
            )),
        }
    } else {
        Err("Failed to authenticate with minecraft using Xbox credentials!".into())
    }
}

pub async fn owns_minecraft(
    client: &reqwest::Client,
    minecraft_auth: &MinecraftAuthResponse,
) -> Result<bool, String> {
    let res = client
        .get("https://api.minecraftservices.com/entitlements/mcstore")
        .bearer_auth(&minecraft_auth.access_token)
        .send()
        .await;

    if let Ok(response) = res {
        match response.status() {
            StatusCode::OK => match response.json::<MinecraftOwnershipResponse>().await {
                Ok(response) => {
                    let items = response.items;

                    let mut product_minecraft_found = false;
                    let mut game_minecraft_found = false;
                    for item in &items {
                        match item.name.as_str() {
                            "product_minecraft" => product_minecraft_found = true,
                            "game_minecraft" => game_minecraft_found = true,
                            _ => {}
                        }
                    }
                    Ok(product_minecraft_found && game_minecraft_found)
                }
                Err(err) => Err(err.to_string()),
            },
            StatusCode::TOO_MANY_REQUESTS => {
                println!("{:?}", response);
                Err(format!(
                    "Too many attempts to check Minecraft ownership, please try again later: {:?}",
                    response
                ))
            }
            _ => Err(format!(
                "Failed to validate the microsoft account owns the game: Minecraft!"
            )),
        }
    } else {
        Err("Failed to validate the microsoft account owns the game: Minecraft!".into())
    }
}

pub async fn minecraft_profile(
    client: &reqwest::Client,
    minecraft_auth: &MinecraftAuthResponse,
) -> Result<MinecraftProfile, String> {
    println!("Getting profile");
    let res = client
        .get("https://api.minecraftservices.com/minecraft/profile")
        .bearer_auth(&minecraft_auth.access_token)
        .send()
        .await;

    println!("Access token: {:?}", &minecraft_auth.access_token);

    if let Ok(response) = res {
        match response.status() {
            StatusCode::OK => match response.json::<MinecraftProfile>().await {
                Ok(response) => Ok(response),
                Err(err) => Err(err.to_string()),
            },
            _ => Err(format!("Account retrieval response was bad: {:?}", response).into()),
        }
    } else {
        Err("Failed to retrieve Minecraft account profile!".into())
    }
}
