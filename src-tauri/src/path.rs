use std::path::PathBuf;

use tauri::Manager;

const TETRAMC_DIR: &'static str = "tetramc";
const TETRAMC_JAVA_DIR: &'static str = "tetramc/java";
const TETRAMC_INSTANCE_DIR: &'static str = "tetramc/instances";
const TETRAMC_ASSET_DIR: &'static str = "tetramc/assets";
const TETRAMC_ASSET_OBJECT_DIR: &'static str = "tetramc/assets/objects";
const TETRAMC_ASSET_INDEX_DIR: &'static str = "tetramc/assets/indexes";
const TETRAMC_VIRTUAL_ASSET_OBJECT_DIR: &'static str = "tetramc/assets/virtual";
const TETRAMC_LIBRARIES_DIR: &'static str = "tetramc/libraries";
const TETRAMC_CLIENT_DOWNLOAD_DIR: &'static str = "tetramc/clients";
const TETRAMC_RESOURCES_ASSET_OBJECT_DIR: &'static str = ".minecraft/resources";

const ERR_NO_ACCESS_DATA_DIR: &'static str = "No access to the local data directory!";
const ERR_NO_ACCESS_CACHE_DIR: &'static str = "No access to the cache directory!";
const ERR_NO_ACCESS_CONFIG_DIR: &'static str = "No access to the config directory!";

// pub const TETRAMC_LAUNCHER_NAME: &'static str = "TetraMC";
// pub const TETRAMC_LAUNCHER_VERSION: &'static str = "0.1.0";

pub fn tetramc_cache_dir(app: &tauri::AppHandle) -> Result<PathBuf, &'static str> {
    match app.path().cache_dir() {
        Ok(mut cache_dir) => {
            cache_dir.push(TETRAMC_DIR);
            Ok(cache_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_CACHE_DIR),
    }
}

pub fn tetramc_config_dir(app: &tauri::AppHandle) -> Result<PathBuf, &'static str> {
    match app.path().config_dir() {
        Ok(config_dir) => Ok(config_dir),
        Err(_) => Err(ERR_NO_ACCESS_CONFIG_DIR),
    }
}

pub fn tetramc_local_data_dir(app: &tauri::AppHandle) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_DIR);
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_path_to_instances(app: &tauri::AppHandle) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_DIR);
            local_data_dir.push("instances");
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_instance_dir(
    app: &tauri::AppHandle,
    instance_name: impl AsRef<str>,
) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_DIR);
            local_data_dir.push("instances");
            local_data_dir.push(instance_name.as_ref());
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_instance_subdir(
    app: &tauri::AppHandle,
    instance_name: impl AsRef<str>,
    subdir_name: impl AsRef<str>,
) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_DIR);
            local_data_dir.push("instances");
            local_data_dir.push(instance_name.as_ref());
            local_data_dir.push(subdir_name.as_ref());
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_client_dir(
    app: &tauri::AppHandle,
    client_name: impl AsRef<str>,
) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_DIR);
            local_data_dir.push("clients");
            local_data_dir.push(client_name.as_ref());
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_client_subdir(
    app: &tauri::AppHandle,
    client_name: impl AsRef<str>,
    subdir_name: impl AsRef<str>,
) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_DIR);
            local_data_dir.push("clients");
            local_data_dir.push(client_name.as_ref());
            local_data_dir.push(subdir_name.as_ref());
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_java_install_dir(
    app: &tauri::AppHandle,
    java_install_dir: Option<&str>,
) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_JAVA_DIR);
            if let Some(java_install_dir) = java_install_dir {
                local_data_dir.push(java_install_dir);
            }
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_virtual_asset_object_dir(app: &tauri::AppHandle) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_VIRTUAL_ASSET_OBJECT_DIR);
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_resources_asset_object_dir(
    app: &tauri::AppHandle,
    instance_name: impl AsRef<str>,
) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_INSTANCE_DIR);
            local_data_dir.push(instance_name.as_ref());
            local_data_dir.push(TETRAMC_RESOURCES_ASSET_OBJECT_DIR);
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_asset_object_dir(app: &tauri::AppHandle) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_ASSET_OBJECT_DIR);
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_asset_index_dir(app: &tauri::AppHandle) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_ASSET_INDEX_DIR);
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_assets_dir(app: &tauri::AppHandle) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_ASSET_DIR);
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_libraries_dir(app: &tauri::AppHandle) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_LIBRARIES_DIR);
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}

pub fn tetramc_natives_dir(
    app: &tauri::AppHandle,
    instance_name: impl AsRef<str>,
) -> Result<PathBuf, &'static str> {
    match app.path().local_data_dir() {
        Ok(mut local_data_dir) => {
            local_data_dir.push(TETRAMC_INSTANCE_DIR);
            local_data_dir.push(instance_name.as_ref());
            local_data_dir.push(".minecraft");
            local_data_dir.push("natives");
            Ok(local_data_dir)
        }
        Err(_) => Err(ERR_NO_ACCESS_DATA_DIR),
    }
}
