use std::collections::HashMap;

use crate::instance::fabric::models::FabricArguments;

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftVersionLatest {
    pub release: String,
    pub snapshot: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MinecraftVersion {
    pub id: String,
    #[serde(alias = "type")]
    pub ty: String,
    pub url: String,
    pub time: String,
    pub release_time: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftVersionResponse {
    pub latest: MinecraftVersionLatest,
    pub versions: Vec<MinecraftVersion>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct MinecraftInstanceAssetIndex {
    pub id: String,
    pub sha1: String,
    pub size: i64,
    pub total_size: i64,
    pub url: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MinecraftInstanceDownloadsClient {
    pub sha1: String,
    pub size: i64,
    pub url: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftInstanceDownloads {
    pub client: MinecraftInstanceDownloadsClient,
    pub client_mappings: Option<MinecraftInstanceDownloadsClient>,
    pub server: Option<MinecraftInstanceDownloadsClient>,
    pub server_mappings: Option<MinecraftInstanceDownloadsClient>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MinecraftInstanceJavaVersion {
    pub component: String,
    pub major_version: i64,
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct MinecraftInstanceLibraryDownloadsArtifact {
    pub path: String,
    pub sha1: String,
    pub size: i64,
    pub url: String,
}

#[derive(Debug, Clone, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub struct MinecraftInstanceLibraryClassifiers {
    #[serde(alias = "natives-linux")]
    pub natives_linux: Option<MinecraftInstanceLibraryDownloadsArtifact>,
    #[serde(alias = "natives-osx")]
    pub natives_osx: Option<MinecraftInstanceLibraryDownloadsArtifact>,
    #[serde(alias = "natives-windows")]
    pub natives_windows: Option<MinecraftInstanceLibraryDownloadsArtifact>,
    pub javadoc: Option<MinecraftInstanceLibraryDownloadsArtifact>,
    pub sources: Option<MinecraftInstanceLibraryDownloadsArtifact>,
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct MinecraftInstanceLibraryDownloads {
    pub artifact: Option<MinecraftInstanceLibraryDownloadsArtifact>,
    pub classifiers: Option<MinecraftInstanceLibraryClassifiers>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftInstanceLibraryRuleOs {
    pub name: String,
}

#[derive(Debug, Clone, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub struct MinecraftInstanceLibraryExtract {
    pub exclude: Vec<String>,
}

#[derive(Debug, Clone, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub struct MinecraftInstanceLibraryNatives {
    pub linux: Option<String>,
    pub windows: Option<String>,
    pub osx: Option<String>,
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct MinecraftInstanceLibrary {
    pub downloads: MinecraftInstanceLibraryDownloads,
    pub extract: Option<MinecraftInstanceLibraryExtract>,
    pub natives: Option<MinecraftInstanceLibraryNatives>,
    pub name: String,
    pub rules: Option<Vec<MinecraftInstanceRule>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftInstanceLoggingFile {
    pub id: String,
    pub sha1: String,
    pub size: i64,
    pub url: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftInstanceLoggingClient {
    pub argument: String,
    pub file: MinecraftInstanceLoggingFile,
    #[serde(alias = "type")]
    pub ty: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftInstanceLogging {
    pub client: MinecraftInstanceLoggingClient,
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize, Clone)]
pub struct MinecraftInstanceRuleOs {
    pub name: Option<String>,
    pub version: Option<String>,
    pub arch: Option<String>,
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize, Clone)]
pub struct MinecraftInstanceRuleFeature {
    pub is_demo_user: Option<bool>,
    pub has_custom_resolution: Option<bool>,
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize, Clone)]
pub struct MinecraftInstanceRule {
    pub action: Option<String>,
    pub os: Option<MinecraftInstanceRuleOs>,
    pub features: Option<MinecraftInstanceRuleFeature>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum MinecraftInstanceArgument {
    RuleWithMultiple {
        rules: Vec<MinecraftInstanceRule>,
        value: Vec<String>,
    },
    RuleWithSingle {
        rules: Vec<MinecraftInstanceRule>,
        value: String,
    },
    Value(String),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum MinecraftInstanceArguments {
    Together(String),
    Separated {
        game: Vec<MinecraftInstanceArgument>,
        jvm: Vec<MinecraftInstanceArgument>,
    },
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftInstanceAsset {
    pub hash: String,
    pub size: i64,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftInstanceAssetIndexObject {
    pub objects: HashMap<String, MinecraftInstanceAsset>,
    #[serde(alias = "virtual")]
    pub virt: Option<bool>,
    pub map_to_resources: Option<bool>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MinecraftVersionInfoResponse {
    #[serde(alias = "minecraftArguments")]
    pub arguments: MinecraftInstanceArguments,
    pub asset_index: MinecraftInstanceAssetIndex,
    pub assets: String,
    pub compliance_level: i64,
    pub downloads: MinecraftInstanceDownloads,
    pub id: String,
    pub java_version: MinecraftInstanceJavaVersion,
    pub libraries: Vec<MinecraftInstanceLibrary>,
    pub logging: Option<MinecraftInstanceLogging>,
    pub main_class: String,
    pub minimum_launcher_version: i64,
    pub release_time: String,
    pub time: String,
    #[serde(alias = "type")]
    pub ty: String,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum TetraMCInstanceType {
    Vanilla,
    Fabric,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AddOn {
    pub project_id: String,
    pub version_id: Option<String>,
    pub version_name: Option<String>,
    pub file_name: Option<String>,
    pub platform: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TetraMCInstance {
    pub name: String,
    pub created: DateTime<Utc>,
    pub last_played: Option<DateTime<Utc>>,
    pub arguments: MinecraftInstanceArguments,
    pub fabric_arguments: Option<FabricArguments>,
    pub main_class: String,
    pub id: String,
    pub assets: String,
    pub asset_index: MinecraftInstanceAssetIndex,
    pub ty: TetraMCInstanceType,
    pub client_path: String,
    pub virtual_assets: bool,
    pub map_to_resources: bool,
    pub mods: Option<Vec<AddOn>>,
    pub java_path: Option<String>,
    pub natives_path: Option<String>,
    pub ram_min: Option<usize>,
    pub ram_max: Option<usize>,
    pub client_log_config: Option<String>,
    pub libraries: Vec<String>,
}
