use std::path::PathBuf;

use reqwest::StatusCode;
use tauri::Emitter;

use crate::{
    models::DownloadStatusPayload,
    path::{
        tetramc_asset_index_dir, tetramc_asset_object_dir, tetramc_resources_asset_object_dir,
        tetramc_virtual_asset_object_dir,
    },
};

use super::models::{MinecraftInstanceAssetIndexObject, MinecraftVersionInfoResponse};

const VANILLA_ASSET_OBJECT_URL: &'static str = "https://resources.download.minecraft.net";

#[derive(Debug)]
enum AssetIndexType {
    Virtual(String),
    Map(String),
    Object,
}

pub async fn download_assets(
    app: &tauri::AppHandle,
    client: &tauri::State<'_, reqwest::Client>,
    minecraft_version_info_response: &MinecraftVersionInfoResponse,
    instance_name: impl AsRef<str>,
) -> Result<(bool, bool), String> {
    let asset_index = retrieve_asset_index(client, minecraft_version_info_response).await?;

    let is_virtual = asset_index.virt.unwrap_or_default();
    let map_to_resources = asset_index.map_to_resources.unwrap_or_default();

    let asset_index_type = if is_virtual {
        AssetIndexType::Virtual(minecraft_version_info_response.asset_index.id.to_owned())
    } else if map_to_resources {
        AssetIndexType::Map(instance_name.as_ref().to_string())
    } else {
        AssetIndexType::Object
    };

    download_asset_files(&app, client, asset_index_type, &asset_index).await?;

    download_asset_index_file(&app, client, &minecraft_version_info_response).await?;

    Ok((is_virtual, map_to_resources))
}

async fn download_asset_index_file(
    app: &tauri::AppHandle,
    client: &tauri::State<'_, reqwest::Client>,
    minecraft_version_info_response: &MinecraftVersionInfoResponse,
) -> Result<(), String> {
    let mut asset_index_dir = tetramc_asset_index_dir(&app)?;

    if !asset_index_dir.exists() {
        std::fs::create_dir_all(&asset_index_dir)
            .map_err(|e| format!("Failed to create asset index directory: {:?}", e))?;
    }

    asset_index_dir.push(format!(
        "{}.json",
        minecraft_version_info_response.asset_index.id
    ));

    app.emit(
        "download-instance-status",
        DownloadStatusPayload {
            message: String::from("Downloading asset index file."),
            percent: 0.0,
        },
    )
    .map_err(|e| {
        format!(
            "Failed to emit asset download message to front-end: {:?}",
            e
        )
    })?;

    crate::file::download_file(
        client,
        &minecraft_version_info_response.asset_index.url,
        &asset_index_dir,
    )
    .await?;

    app.emit(
        "download-instance-status",
        DownloadStatusPayload {
            message: String::from("Downloading asset index file."),
            percent: 100.0,
        },
    )
    .map_err(|e| {
        format!(
            "Failed to emit asset download message to front-end: {:?}",
            e
        )
    })?;

    Ok(())
}

async fn download_asset_files(
    app: &tauri::AppHandle,
    client: &tauri::State<'_, reqwest::Client>,
    asset_index_type: AssetIndexType,
    asset_index: &MinecraftInstanceAssetIndexObject,
) -> Result<(), String> {
    let target_dir = get_asset_index_dir_for_type(&app, &asset_index_type)?;
    if !target_dir.exists() {
        std::fs::create_dir_all(&target_dir).map_err(|e| {
            format!(
                "Failed to create asset file directory `{:?}`: {:?}",
                target_dir, e
            )
        })?;
    }

    let total_object_count = asset_index.objects.len();
    for (i, (key, value)) in asset_index.objects.iter().enumerate() {
        let mut target_dir_clone = target_dir.clone();
        match &asset_index_type {
            AssetIndexType::Virtual(_) => {
                target_dir_clone.push(key);
            }
            AssetIndexType::Object => {
                target_dir_clone.push(format!("{}/{}", &value.hash[0..2], value.hash));
            }
            AssetIndexType::Map(_) => {
                target_dir_clone.push(key);
            }
        }

        let parent = target_dir_clone
            .parent()
            .ok_or("Invalid asset object path, has no parent!".to_string())?;

        if !parent.exists() {
            std::fs::create_dir_all(parent).map_err(|e| {
                format!(
                    "Failed to create asset object directory `{:?}`: {:?}",
                    parent, e
                )
            })?;
        }

        if !target_dir_clone.exists() {
            app.emit(
                "download-instance-status",
                DownloadStatusPayload {
                    message: format!(
                        "Downloading: {} of {} asset files. ({})",
                        i + 1,
                        total_object_count,
                        key
                    ),
                    percent: (i as f64 / total_object_count as f64) * 100.0,
                },
            )
            .map_err(|e| {
                format!(
                    "Failed to emit asset download message to front-end: {:?}",
                    e
                )
            })?;

            crate::file::download_file(
                client,
                format!(
                    "{}/{}/{}",
                    VANILLA_ASSET_OBJECT_URL,
                    &value.hash[0..2],
                    value.hash
                ),
                &target_dir_clone,
            )
            .await?;
        }
    }

    Ok(())
}

fn get_asset_index_dir_for_type(
    app: &tauri::AppHandle,
    asset_index_type: &AssetIndexType,
) -> Result<PathBuf, String> {
    match asset_index_type {
        AssetIndexType::Virtual(id) => {
            let mut dir = tetramc_virtual_asset_object_dir(&app)?;
            dir.push(&id);
            Ok(dir)
        }
        AssetIndexType::Map(instance_name) => {
            match tetramc_resources_asset_object_dir(&app, instance_name) {
                Ok(dir) => Ok(dir),
                Err(err) => Err(err.to_string()),
            }
        }
        AssetIndexType::Object => match tetramc_asset_object_dir(&app) {
            Ok(dir) => Ok(dir),
            Err(err) => Err(err.to_string()),
        },
    }
}

async fn retrieve_asset_index(
    client: &reqwest::Client,
    minecraft_version_info_response: &MinecraftVersionInfoResponse,
) -> Result<MinecraftInstanceAssetIndexObject, String> {
    let res = client
        .get(&minecraft_version_info_response.asset_index.url)
        .send()
        .await
        .map_err(|e| format!("Failed to retrieve minecraft asset index: {:?}", e))?;

    if res.status() == StatusCode::OK {
        Ok(res
            .json::<MinecraftInstanceAssetIndexObject>()
            .await
            .map_err(|e| format!("Failed to parse minecraft asset index: {:?}", e))?)
    } else {
        Err("Failed to retrieve minecraft asset index!".to_string())?
    }
}
