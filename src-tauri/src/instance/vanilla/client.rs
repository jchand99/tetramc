use std::{io::Write, path::PathBuf};

use crate::path::{
    tetramc_client_dir, tetramc_client_subdir, tetramc_instance_dir, tetramc_instance_subdir,
};

use super::models::MinecraftVersionInfoResponse;

pub async fn retrieve_minecraft_version_data(
    client: &tauri::State<'_, reqwest::Client>,
    url: &str,
) -> Result<MinecraftVersionInfoResponse, String> {
    Ok(client
        .get(url)
        .send()
        .await
        .map_err(|e| {
            format!(
                "Failed to connect to minecraft version data source: {:?}",
                e
            )
        })?
        .json::<MinecraftVersionInfoResponse>()
        .await
        .map_err(|e| format!("Failed to retrieve minecraft version information: {:?}", e))?)
}

pub async fn download_client(
    app: &tauri::AppHandle,
    client: &reqwest::Client,
    minecraft_version_info_response: &MinecraftVersionInfoResponse,
    instance_name: &str,
) -> Result<PathBuf, String> {
    create_instance_directory(&app, instance_name)?;
    create_client_dir(&app, minecraft_version_info_response)?;

    let client_path =
        download_client_jar_file(&app, minecraft_version_info_response, client).await?;
    // write_client_version_json(minecraft_version_info_response)?;
    download_client_log_config_file(&app, instance_name, minecraft_version_info_response, client)
        .await?;

    Ok(client_path)
}

async fn download_client_log_config_file(
    app: &tauri::AppHandle,
    instance_name: &str,
    minecraft_version_info_response: &MinecraftVersionInfoResponse,
    client: &reqwest::Client,
) -> Result<(), String> {
    if let Some(logging) = &minecraft_version_info_response.logging {
        let instance_log_config_file =
            tetramc_instance_subdir(&app, instance_name, &logging.client.file.id)?;

        crate::file::download_file(&client, &logging.client.file.url, &instance_log_config_file)
            .await?;
        Ok(())
    } else {
        Ok(())
    }
}

fn write_client_version_json(
    app: &tauri::AppHandle,
    minecraft_version_info_response: &MinecraftVersionInfoResponse,
) -> Result<(), String> {
    let version_info_path = tetramc_instance_subdir(
        &app,
        &minecraft_version_info_response.id,
        format!("version_info.json"),
    )?;

    if !version_info_path.exists() {
        let mut file = std::fs::File::create(&version_info_path)
            .map_err(|e| format!("Failed to create client version json file: {:?}", e))?;

        let text =
            serde_json::to_string::<MinecraftVersionInfoResponse>(&minecraft_version_info_response)
                .map_err(|e| {
                    format!(
                        "Failed to parse minecraft version info response to json: {:?}",
                        e
                    )
                })?;

        file.write_all(text.as_bytes())
            .map_err(|e| format!("Failed to write client version json file: {:?}", e))?;
    }
    Ok(())
}

async fn download_client_jar_file(
    app: &tauri::AppHandle,
    minecraft_version_info_response: &MinecraftVersionInfoResponse,
    client: &reqwest::Client,
) -> Result<PathBuf, String> {
    let client_jar_file = tetramc_client_subdir(
        &app,
        &minecraft_version_info_response.id,
        format!(
            "minecraft-{}-client.jar",
            &minecraft_version_info_response.id
        ),
    )?;
    if !client_jar_file.exists() {
        crate::file::download_file(
            &client,
            &minecraft_version_info_response.downloads.client.url,
            &client_jar_file,
        )
        .await?;
    }

    Ok(client_jar_file)
}

fn create_instance_directory(app: &tauri::AppHandle, instance_name: &str) -> Result<(), String> {
    let instance_path = tetramc_instance_dir(&app, instance_name)?;
    if !instance_path.exists() {
        std::fs::create_dir_all(&instance_path)
            .map_err(|e| format!("Failed to create instance directory: {:?}", e))?;
    }

    Ok(())
}

fn create_client_dir(
    app: &tauri::AppHandle,
    minecraft_version_info_response: &MinecraftVersionInfoResponse,
) -> Result<(), String> {
    let client_path = tetramc_client_dir(&app, &minecraft_version_info_response.id)?;

    if !client_path.exists() {
        std::fs::create_dir_all(&client_path)
            .map_err(|e| format!("Failed to create client directory: {:?}", e))?;
    }

    Ok(())
}
