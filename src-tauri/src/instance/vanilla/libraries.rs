use std::collections::BTreeSet;
use tauri::Emitter;

use crate::instance::java;
use crate::{file::download_file, models::DownloadStatusPayload};
use std::path::PathBuf;

use crate::path::{tetramc_libraries_dir, tetramc_natives_dir};

use super::models::{
    MinecraftInstanceLibrary, MinecraftInstanceLibraryClassifiers,
    MinecraftInstanceLibraryDownloadsArtifact, MinecraftInstanceRule, MinecraftVersionInfoResponse,
};

const LOG4J_UPDATED_WITHOUT_EXPLOIT_API_URL: &'static str = "https://libraries.minecraft.net/org/apache/logging/log4j/log4j-api/2.17.0/log4j-api-2.17.0.jar";
const LOG4J_UPDATED_WITHOUT_EXPLOIT_PATH_API: &'static str =
    "org/apache/logging/log4j/log4j-api/2.17.0/log4j-api-2.17.0.jar";
const LOG4J_UPDATED_WITHOUT_EXPLOIT_CORE_URL: &'static str = "https://libraries.minecraft.net/org/apache/logging/log4j/log4j-core/2.17.0/log4j-core-2.17.0.jar";
const LOG4J_UPDATED_WITHOUT_EXPLOIT_PATH_CORE: &'static str =
    "org/apache/logging/log4j/log4j-core/2.17.0/log4j-core-2.17.0.jar";

pub async fn download_libraries(
    app: &tauri::AppHandle,
    client: &reqwest::Client,
    minecraft_version_info_response: &mut MinecraftVersionInfoResponse,
    instance_name: &str,
) -> Result<Option<String>, String> {
    let libraries_dir = tetramc_libraries_dir(&app)?;
    let natives_dir = tetramc_natives_dir(&app, instance_name)?;

    if !libraries_dir.exists() {
        std::fs::create_dir_all(&libraries_dir)
            .map_err(|e| format!("Failed to create libraries directory: {:?}", e))?;
    }
    if !natives_dir.exists() {
        std::fs::create_dir_all(&natives_dir)
            .map_err(|e| format!("Failed to create natives directory: {:?}", e))?;
    }

    let total_library_artifact_count = minecraft_version_info_response.libraries.len();

    let mut has_natives = false;
    for (i, library) in minecraft_version_info_response
        .libraries
        .iter_mut()
        .enumerate()
    {
        let mut libraries_dir_clone = libraries_dir.clone();
        let mut natives_dir_clone = natives_dir.clone();
        app.emit(
            "download-instance-status",
            DownloadStatusPayload {
                message: format!(
                    "Downloading: {} of {} library files.",
                    i + 1,
                    total_library_artifact_count
                ),
                percent: (i as f64 / total_library_artifact_count as f64) * 100.0,
            },
        )
        .map_err(|e| format!("Failed to download library artifact: {:?}", e))?;

        // FIXME: Make this whole library process simpler by just downloading the artifacts that are supported by
        // the current platform version.
        if let Some(artifact) = &mut library.downloads.artifact {
            download_artifact(
                client,
                &minecraft_version_info_response.id,
                library.name.as_str(),
                artifact,
                &mut libraries_dir_clone,
            )
            .await?;
        }

        if let Some(classifiers) = &library.downloads.classifiers {
            let were_natives_present =
                download_classifier(&app, client, library, classifiers, &mut natives_dir_clone)
                    .await?;
            has_natives = has_natives || were_natives_present;
        }
    }

    app.emit(
        "download-instance-status",
        DownloadStatusPayload {
            message: String::from("Download complete!"),
            percent: 100.0,
        },
    )
    .map_err(|e| format!("Failed to emit message to window: {:?}", e))?;

    if has_natives {
        Ok(Some(
            natives_dir
                .to_str()
                .ok_or("Failed to convert natives_directory to string".to_string())?
                .to_string(),
        ))
    } else {
        Ok(None)
    }
}

#[cfg(target_os = "linux")]
async fn download_classifier(
    app: &tauri::AppHandle,
    client: &reqwest::Client,
    library: &MinecraftInstanceLibrary,
    classifiers: &MinecraftInstanceLibraryClassifiers,
    natives_dir: &mut PathBuf,
) -> Result<bool, String> {
    // If there are no natives available for the linux platform we want to just return.

    if !library
        .natives
        .as_ref()
        .is_some_and(|natives| natives.linux.is_some())
    {
        return Ok(false);
    }

    if let Some(natives) = &classifiers.natives_linux {
        natives_dir.push(natives.path.split('/').last().unwrap());
        download_file(client, &natives.url, &natives_dir).await?;
        java::extract_jar(&app, &natives_dir)?;
        // TODO: Delete jar file?
        natives_dir.pop();
    }

    if let Some(natives) = &classifiers.javadoc {
        natives_dir.push(natives.path.split('/').last().unwrap());
        download_file(client, &natives.url, &natives_dir).await?;
        java::extract_jar(&app, &natives_dir)?;
        // TODO: Delete jar file?
        natives_dir.pop();
    }

    if let Some(natives) = &classifiers.sources {
        natives_dir.push(natives.path.split('/').last().unwrap());
        download_file(client, &natives.url, &natives_dir).await?;
        java::extract_jar(&app, &natives_dir)?;
        // TODO: Delete jar file?
        natives_dir.pop();
    }

    Ok(true)
}

#[cfg(target_os = "windows")]
async fn download_classifier(
    app: &tauri::AppHandle,
    client: &reqwest::Client,
    library: &MinecraftInstanceLibrary,
    classifiers: &MinecraftInstanceLibraryClassifiers,
    natives_dir: &mut PathBuf,
) -> Result<bool, String> {
    // If there are no natives available for the windows platform we want to just return.
    if !library
        .natives
        .as_ref()
        .is_some_and(|natives| natives.windows.is_some())
    {
        return Ok(false);
    }

    if let Some(natives) = &classifiers.natives_windows {
        natives_dir.push(natives.path.split('/').last().unwrap());
        download_file(client, &natives.url, &natives_dir).await?;
        java::extract_jar(&app, &natives_dir)?;
        natives_dir.pop();
    }

    if let Some(natives) = &classifiers.javadoc {
        natives_dir.push(natives.path.split('/').last().unwrap());
        download_file(client, &natives.url, &natives_dir).await?;
        java::extract_jar(&app, &natives_dir)?;
        natives_dir.pop();
    }

    if let Some(natives) = &classifiers.sources {
        natives_dir.push(natives.path.split('/').last().unwrap());
        download_file(client, &natives.url, &natives_dir).await?;
        java::extract_jar(&app, &natives_dir)?;
        natives_dir.pop();
    }

    Ok(true)
}

#[cfg(target_os = "macos")]
async fn download_classifier(
    app: &tauri::AppHandle,
    client: &reqwest::Client,
    library: &MinecraftInstanceLibrary,
    classifiers: &MinecraftInstanceLibraryClassifiers,
    natives_dir: &mut PathBuf,
) -> Result<bool, String> {
    // If there are no natives available for the macos platform we want to just return.
    if !library
        .natives
        .as_ref()
        .is_some_and(|natives| natives.osx.is_some())
    {
        return Ok(false);
    }

    if let Some(natives) = &classifiers.natives_osx {
        natives_dir.push(natives.path.split('/').last().unwrap());
        download_file(client, &natives.url, &natives_dir).await?;
        java::extract_jar(&app, &natives_dir)?;
        natives_dir.pop();
    }

    if let Some(natives) = &classifiers.javadoc {
        natives_dir.push(natives.path.split('/').last().unwrap());
        download_file(client, &natives.url, &natives_dir).await?;
        java::extract_jar(&app, &natives_dir)?;
        natives_dir.pop();
    }

    if let Some(natives) = &classifiers.sources {
        natives_dir.push(natives.path.split('/').last().unwrap());
        download_file(client, &natives.url, &natives_dir).await?;
        java::extract_jar(&app, &natives_dir)?;
        natives_dir.pop();
    }

    Ok(true)
}

async fn download_artifact(
    client: &reqwest::Client,
    version: impl AsRef<str>,
    library_name: impl AsRef<str>,
    artifact: &mut MinecraftInstanceLibraryDownloadsArtifact,
    libraries_dir: &mut PathBuf,
) -> Result<(), String> {
    // FIXME: This is still ugly
    // and has the issue with snapshot versions (no '.' character).
    let url = if version
        .as_ref()
        .split(".")
        .nth(1)
        .unwrap_or_default()
        .parse::<u16>()
        .unwrap_or_default()
        <= 8
    {
        if library_name.as_ref().contains("log4j-api") {
            libraries_dir.push(LOG4J_UPDATED_WITHOUT_EXPLOIT_PATH_API);
            fix_log4j_api(library_name, artifact)?
        } else if library_name.as_ref().contains("log4j-core") {
            libraries_dir.push(LOG4J_UPDATED_WITHOUT_EXPLOIT_PATH_CORE);
            fix_log4j_core(library_name, artifact)?
        } else {
            libraries_dir.push(&artifact.path);
            artifact.url.as_str()
        }
    } else {
        libraries_dir.push(&artifact.path);
        artifact.url.as_str()
    };

    if !libraries_dir.exists() {
        let parent = libraries_dir
            .parent()
            .ok_or("Unable to get library parent path.".to_string())?;
        std::fs::create_dir_all(parent)
            .map_err(|e| format!("Failed to create libraries directory: {:?}", e))?;

        crate::file::download_file(client, url, &libraries_dir).await?;
    }

    Ok(())
}

fn fix_log4j_api<'a>(
    library_name: impl AsRef<str>,
    artifact: &'a mut MinecraftInstanceLibraryDownloadsArtifact,
) -> Result<&'a str, String> {
    let version = library_name.as_ref().split(":").last().unwrap();
    let version = version
        .split(".")
        .skip(1)
        .next()
        .ok_or("Invalid library, could not retrieve version".to_string())?
        .split("-")
        .next()
        .ok_or("Invalid library, could not retrieve version".to_string())?
        .parse::<usize>()
        .map_err(|e| format!("Unable to parse library version: {:?}", e))?;

    if version < 16 {
        artifact.path = LOG4J_UPDATED_WITHOUT_EXPLOIT_PATH_API.to_owned();
        artifact.url = LOG4J_UPDATED_WITHOUT_EXPLOIT_API_URL.to_owned();
        Ok(LOG4J_UPDATED_WITHOUT_EXPLOIT_API_URL)
    } else {
        Ok(artifact.url.as_str())
    }
}

fn fix_log4j_core<'a>(
    library_name: impl AsRef<str>,
    artifact: &'a mut MinecraftInstanceLibraryDownloadsArtifact,
) -> Result<&'a str, String> {
    let version = library_name.as_ref().split(":").last().unwrap();
    let version = version
        .split(".")
        .skip(1)
        .next()
        .ok_or("Invalid library, could not retrieve version".to_string())?
        .split("-")
        .next()
        .ok_or("Invalid library, could not retrieve version".to_string())?
        .parse::<usize>()
        .map_err(|e| format!("Unable to parse library version: {:?}", e))?;

    if version < 16 {
        artifact.path = LOG4J_UPDATED_WITHOUT_EXPLOIT_PATH_CORE.to_owned();
        artifact.url = LOG4J_UPDATED_WITHOUT_EXPLOIT_CORE_URL.to_owned();
        Ok(LOG4J_UPDATED_WITHOUT_EXPLOIT_CORE_URL)
    } else {
        Ok(artifact.url.as_str())
    }
}

// FIXME: Might be able to get rid of this if we just download the libraries that are supported
// by the current platform by default. Check download libraries function FIXME for more info.
pub fn get_list_of_libraries(
    minecraft_version_info_response: &MinecraftVersionInfoResponse,
) -> Result<Vec<MinecraftInstanceLibrary>, String> {
    Ok(Vec::from_iter(
        minecraft_version_info_response
            .libraries
            .iter()
            .filter(|lib| lib.downloads.artifact.is_some())
            .map(|lib| match &lib.rules {
                Some(rules) => {
                    if get_disallow_rules_count(&rules) == 0 {
                        Some(lib.clone())
                    } else {
                        None
                    }
                }
                None => Some(lib.clone()),
            })
            .flatten()
            .collect::<BTreeSet<MinecraftInstanceLibrary>>()
            .into_iter(),
    ))
}

#[cfg(target_os = "linux")]
fn get_disallow_rules_count(rules: &[MinecraftInstanceRule]) -> usize {
    rules
        .iter()
        .filter(|&rule| {
            let not_allowed = match &rule.action {
                Some(action) => action == "disallow",
                None => false,
            };
            let linux_not_allowed = match &rule.os {
                Some(os) => match &os.name {
                    Some(name) => name == "linux",
                    None => false,
                },
                None => false,
            };

            not_allowed && linux_not_allowed
        })
        .count()
}

#[cfg(target_os = "windows")]
fn get_disallow_rules_count(rules: &[MinecraftInstanceRule]) -> usize {
    rules
        .iter()
        .filter(|&rule| {
            let not_allowed = match &rule.action {
                Some(action) => action == "disallow",
                None => false,
            };
            let linux_not_allowed = match &rule.os {
                Some(os) => match &os.name {
                    Some(name) => name == "windows",
                    None => false,
                },
                None => false,
            };

            not_allowed && linux_not_allowed
        })
        .count()
}

#[cfg(target_os = "macos")]
fn get_disallow_rules_count(rules: &[MinecraftInstanceRule]) -> usize {
    rules
        .iter()
        .filter(|&rule| {
            let not_allowed = match &rule.action {
                Some(action) => action == "disallow",
                None => false,
            };
            let linux_not_allowed = match &rule.os {
                Some(os) => match &os.name {
                    Some(name) => name == "osx",
                    None => false,
                },
                None => false,
            };

            not_allowed && linux_not_allowed
        })
        .count()
}
