use std::collections::HashMap;
use tauri_plugin_shell::process::CommandChild;

#[derive(Debug)]
pub struct RunningInstances(HashMap<String, CommandChild>);

impl RunningInstances {
    pub fn new() -> Self {
        Self(HashMap::new())
    }

    pub fn insert(&mut self, name: String, child: CommandChild) {
        self.0.insert(name, child);
    }

    pub fn kill(&mut self, name: impl AsRef<str>) -> Result<(), String> {
        if let Some((name, child)) = self.0.remove_entry(name.as_ref()) {
            Ok(child.kill().map_err(|e| {
                format!(
                    "Failed to kill child minecraft process, instance name `{:?}`: {:?}",
                    name, e
                )
            })?)
        } else {
            Ok(())
        }
    }

    pub fn manually_remove_terminated_child(&mut self, name: impl AsRef<str>) {
        self.0.remove_entry(name.as_ref());
    }
}
