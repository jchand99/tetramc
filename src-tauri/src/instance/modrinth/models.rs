use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub enum ModrinthProjectAction {
    Install {
        project_id: String,
        version_id: String,
        project_title: String,
    },
    Delete {
        project_id: String,
        project_title: String,
    },
    Upgrade {
        file_name: String,
        project_title: String,
        project_id: String,
        from_version_name: String,
        to_version_name: String,
        from_version_id: String,
        to_version_id: String,
    },
    Downgrade {
        file_name: String,
        project_title: String,
        project_id: String,
        from_version_name: String,
        to_version_name: String,
        from_version_id: String,
        to_version_id: String,
    },
    NoOp {
        file_name: String,
        project_title: String,
        project_id: String,
    },
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ModrinthProjectSearchResult {
    pub hits: Vec<ModrinthProjectHit>,
    pub offset: u64,
    pub limit: u64,
    pub total_hits: u64,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ModrinthProjectHit {
    pub project_id: String,
    pub project_type: String,
    pub slug: String,
    pub author: String,
    pub title: String,
    pub description: String,
    pub categories: Vec<String>,
    pub display_categories: Vec<String>,
    pub versions: Vec<String>,
    pub downloads: u64,
    pub follows: u64,
    pub icon_url: String,
    // #[serde(with = "modrinth_date_time_format")]
    pub date_created: String,
    // #[serde(with = "modrinth_date_time_format")]
    pub date_modified: String,
    pub latest_version: String,
    pub client_side: String,
    pub server_side: String,
    pub gallery: Vec<String>,
    pub featured_gallery: Option<String>,
    pub color: Option<u64>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ModrinthProject {
    pub client_side: String,
    pub server_side: String,
    pub game_versions: Vec<String>,
    pub id: String,
    pub slug: String,
    pub project_type: String,
    pub team: String,
    pub organization: Option<String>,
    pub title: String,
    pub description: Option<String>,
    pub body: String,
    pub body_url: Option<String>,
    pub published: String,
    pub updated: String,
    pub approved: Option<String>,
    pub queued: Option<String>,
    pub status: String,
    pub requested_status: Option<String>,
    pub moderator_message: Option<ModrinthProjectMessage>,
    pub license: ModrinthProjectLicense,
    pub downloads: i64,
    pub followers: i64,
    pub categories: Vec<String>,
    pub additional_categories: Vec<String>,
    pub loaders: Vec<String>,
    pub versions: Vec<String>,
    pub icon_url: Option<String>,
    pub issues_url: Option<String>,
    pub source_url: Option<String>,
    pub wiki_url: Option<String>,
    pub discord_url: Option<String>,
    pub donation_urls: Vec<ModrinthProjectDonationUrl>,
    pub gallery: Vec<ModrinthProjectGallery>,
    pub color: Option<i64>,
    pub thread_id: String,
    pub monetization_status: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ModrinthProjectDonationUrl {
    pub id: Option<String>,
    pub platform: Option<String>,
    pub url: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ModrinthProjectGallery {
    pub url: String,
    pub raw_url: String,
    pub featured: bool,
    pub title: Option<String>,
    pub description: Option<String>,
    pub created: String,
    pub ordering: i64,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ModrinthProjectLicense {
    pub id: Option<String>,
    pub name: Option<String>,
    pub url: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ModrinthProjectMessage {
    pub message: Option<String>,
    pub body: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ModrinthProjectVersion {
    pub game_versions: Vec<String>,
    pub loaders: Vec<String>,
    pub id: String,
    pub project_id: String,
    pub author_id: String,
    pub featured: bool,
    pub name: String,
    pub version_number: String,
    pub changelog: String,
    pub changelog_url: Option<String>,
    // #[serde(with = "modrinth_date_time_format")]
    pub date_published: String,
    pub downloads: u64,
    pub version_type: String,
    pub status: String,
    pub requested_status: Option<String>,
    pub files: Vec<ModrinthProjectFile>,
    #[serde(skip_deserializing)] // FIXME: Handle this better.
    pub dependencies: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ModrinthProjectFile {
    pub hashes: ModrinthProjectFileHashes,
    pub url: String,
    pub filename: String,
    pub primary: bool,
    pub size: u64,
    pub file_type: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ModrinthProjectFileHashes {
    sha512: String,
    sha1: String,
}
