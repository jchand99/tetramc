use super::{
    java,
    modrinth::models::ModrinthProjectAction,
    vanilla::{
        self,
        models::{
            MinecraftInstanceArgument, MinecraftInstanceArguments, MinecraftInstanceRuleOs,
            TetraMCInstance, TetraMCInstanceType,
        },
    },
};
use crate::{
    auth::microsoft::MinecraftAccount,
    config::GlobalConfig,
    instance::fabric,
    path::{
        tetramc_assets_dir, tetramc_instance_dir, tetramc_instance_subdir, tetramc_libraries_dir,
        tetramc_natives_dir, tetramc_path_to_instances, tetramc_resources_asset_object_dir,
        tetramc_virtual_asset_object_dir,
    },
};
use chrono::Utc;
use std::path::PathBuf;

pub fn write_instance_toml(
    app: &tauri::AppHandle,
    instance: &TetraMCInstance,
) -> Result<(), String> {
    let instance_path = tetramc_instance_subdir(&app, &instance.name, "tetramc_instance.toml")?;

    let instance_string = toml::to_string::<TetraMCInstance>(&instance).map_err(|e| {
        format!(
            "Failed to convert TetraMCInstance object to string for writing: {:?}",
            e
        )
    })?;

    std::fs::write(&instance_path, &instance_string)
        .map_err(|e| format!("Failed to write TetraMCInstance to TOML file: {:?}", e))?;

    Ok(())
}

pub fn get_instances(app: &tauri::AppHandle) -> Result<Vec<TetraMCInstance>, String> {
    let tetramc_instances_path = tetramc_path_to_instances(&app)?;
    if !tetramc_instances_path.exists() {
        std::fs::create_dir_all(&tetramc_instances_path)
            .map_err(|e| format!("Failed to create instances folder: {:?}", e))?;
    }

    let instance_directories = std::fs::read_dir(&tetramc_instances_path)
        .map_err(|e| format!("Failed to read installed instances: {:?}", e))?
        .filter_map(|e| e.ok());

    let mut installed_instances = vec![];
    for instance in instance_directories {
        let mut path = instance.path();
        path.push("tetramc_instance.toml");

        // FIXME (TODO): Create a function that can send a message to the front end without erroring the function.
        match std::fs::read_to_string(&path) {
            Ok(content) => {
                if let Ok(instance) = toml::from_str::<TetraMCInstance>(&content) {
                    installed_instances.push(instance);
                } else {
                    println!("Failed to convert instance file to TOML object!");
                }
            }
            Err(e) => {
                println!("Failed to read in instance TOML file: {:?}", e);
            }
        };
    }

    installed_instances.sort_by(|a, b| match (a.last_played, b.last_played) {
        (Some(av), Some(bv)) => bv.cmp(&av),
        (None, Some(_)) => std::cmp::Ordering::Greater,
        (Some(_), None) => std::cmp::Ordering::Less,
        (None, None) => b.created.cmp(&a.created),
    });

    Ok(installed_instances)
}

pub fn remove_instance(app: &tauri::AppHandle, instance_name: String) -> Result<(), String> {
    let instance_path = tetramc_instance_dir(&app, instance_name)?;

    std::fs::remove_dir_all(&instance_path)
        .map_err(|e| format!("Failed to remove instance: {:?}", e))?;

    Ok(())
}

pub const DELIMITER: &'static str = "@";
#[cfg(any(target_os = "linux", target_os = "macos"))]
const LIBRARY_DELIMITER: &'static str = ":";
#[cfg(target_os = "windows")]
const LIBRARY_DELIMITER: &'static str = ";";

pub(crate) fn get_game_arguments(
    app: &tauri::AppHandle,
    instance: &TetraMCInstance,
    global_config: &GlobalConfig,
    account: &MinecraftAccount,
    instance_path: PathBuf,
) -> Result<String, String> {
    let ram_args = get_ram_args(instance, global_config);
    let mut args = get_args(app, instance)?;

    let asset_dir = if instance.map_to_resources {
        tetramc_resources_asset_object_dir(&app, &instance.name)?
    } else if instance.virtual_assets {
        tetramc_virtual_asset_object_dir(&app)?
    } else {
        tetramc_assets_dir(&app)?
    };

    let libraries_path = tetramc_libraries_dir(&app)?;
    let mut libraries = instance.libraries.iter().fold(String::new(), |acc, lib| {
        format!(
            "{}{}{}/{}",
            acc,
            LIBRARY_DELIMITER,
            libraries_path.to_str().unwrap_or_default(),
            lib
        )
    });

    libraries.push_str(&format!("{}{}", LIBRARY_DELIMITER, &instance.client_path));
    match instance.ty {
        TetraMCInstanceType::Vanilla => {}
        TetraMCInstanceType::Fabric => {
            // Add fabric arguments to command.
            if let Some(fabric_arguments) = &instance.fabric_arguments {
                args = fabric_arguments
                    .jvm
                    .iter()
                    .fold(args, |acc, arg| format!("{}@{}", arg, acc));
                args = fabric_arguments
                    .game
                    .iter()
                    .fold(args, |acc, arg| format!("{}@{}", acc, arg));
            }
        }
    }

    let instance_type = match instance.ty {
        TetraMCInstanceType::Vanilla => "Vanilla",
        TetraMCInstanceType::Fabric => "Fabric",
    };

    let game_args = format!("{}{}{}", ram_args, DELIMITER, args)
        .replace("${launcher_name}", "TetraMC")
        .replace("${launcher_version}", "0.1.0")
        .replace("${classpath}", &libraries)
        .replace("${mainclass}", &instance.main_class)
        .replace("${auth_player_name}", &account.profile.name)
        .replace("${auth_session}", &account.auth.access_token)
        .replace("${version_name}", &instance.id)
        .replace(
            "${game_directory}",
            instance_path.to_str().unwrap_or_default(),
        )
        .replace("${assets_root}", &asset_dir.to_str().unwrap_or_default())
        .replace("${game_assets}", &asset_dir.to_str().unwrap_or_default())
        .replace("${assets_index_name}", &instance.assets)
        .replace("${auth_uuid}", &account.profile.id)
        .replace("${auth_access_token}", &account.auth.access_token)
        .replace("${user_properties}", "{\"twitch_access_token\": [\"\"]}") // TODO: Add twitch access token ability
        .replace("${user_type}", "msa")
        .replace("${version_type}", instance_type);

    Ok(game_args)
}

fn get_args(app: &tauri::AppHandle, instance: &TetraMCInstance) -> Result<String, String> {
    match &instance.arguments {
        MinecraftInstanceArguments::Together(args) => match &instance.natives_path {
            Some(natives_path) => Ok(format!(
                "-Djava.library.path={}{}-cp{}${{classpath}}{}${{mainclass}}{}{}",
                natives_path,
                DELIMITER,
                DELIMITER,
                DELIMITER,
                DELIMITER,
                args.split_whitespace().collect::<Vec<_>>().join(DELIMITER)
            )),
            None => Ok(format!(
                "-cp{}${{classpath}}{}${{mainclass}}{}{}",
                DELIMITER,
                DELIMITER,
                DELIMITER,
                args.split_whitespace().collect::<Vec<_>>().join(DELIMITER)
            )),
        },
        MinecraftInstanceArguments::Separated { game, jvm } => {
            let mut jvm_args = jvm
                .iter()
                .map(|arg| match arg {
                    MinecraftInstanceArgument::RuleWithMultiple { rules, value } => {
                        rules.iter().fold(String::new(), |acc, rule| {
                            match (&rule.action.as_deref(), &rule.os) {
                                (
                                    Some("allow"),
                                    Some(MinecraftInstanceRuleOs { name: Some(os), .. }),
                                ) if os.as_str() == std::env::consts::OS
                                    || (os.as_str() == "osx"
                                        && std::env::consts::OS == "macos") =>
                                {
                                    format!(
                                        "{}{}",
                                        acc,
                                        value.iter().map(|s| s.as_str()).collect::<String>()
                                    )
                                }
                                _ => acc,
                            }
                        })
                    }
                    MinecraftInstanceArgument::RuleWithSingle { rules, value } => {
                        rules.iter().fold(String::new(), |acc, rule| {
                            // TODO: Fix on mac since std::env::consts::OS = macos and os = osx
                            match (&rule.action.as_deref(), &rule.os) {
                                (
                                    Some("allow"),
                                    Some(MinecraftInstanceRuleOs { name: Some(os), .. }),
                                ) if os.as_str() == std::env::consts::OS => {
                                    format!("{}{}", acc, value)
                                }
                                _ => acc,
                            }
                        })
                    }
                    MinecraftInstanceArgument::Value(value) => value.clone(),
                })
                .filter(|s| s.as_str() != "")
                .collect::<Vec<String>>()
                .join(DELIMITER);

            let game_args = game
                .iter()
                .map(|arg| match arg {
                    MinecraftInstanceArgument::Value(value) => value,
                    _ => "",
                })
                .filter(|&s| s != "")
                .collect::<Vec<_>>()
                .join(DELIMITER);

            let default_natives_path = tetramc_natives_dir(&app, &instance.name)?
                .to_str()
                .unwrap_or_default()
                .to_owned();
            jvm_args = jvm_args.replace(
                "${natives_directory}",
                &instance
                    .natives_path
                    .as_ref()
                    .unwrap_or(&default_natives_path),
            );

            Ok(format!(
                "{}{}{}{}{}",
                jvm_args,
                DELIMITER,
                "${mainclass}".to_string(),
                DELIMITER,
                game_args
            ))
        }
    }
}

fn get_ram_args(instance: &TetraMCInstance, global_config: &GlobalConfig) -> String {
    const MIN_MEM: f64 = 1024.0;

    let mut max = match instance.ram_max {
        Some(max) => max,
        None => global_config.max_mem,
    } as f64;

    if max < MIN_MEM {
        max = MIN_MEM;
    }

    dbg!(format!(
        "-Xmx{}G@-XX:+UnlockExperimentalVMOptions@-XX:+UseG1GC@-XX:G1NewSizePercent=20@-XX:G1ReservePercent=20@-XX:MaxGCPauseMillis=50@-XX:G1HeapRegionSize=32M",
        (max / MIN_MEM) as usize
    ))
}

pub(crate) async fn download_instance(
    app: &tauri::AppHandle,
    client: &tauri::State<'_, reqwest::Client>,
    url: String,
    instance_name: impl AsRef<str>,
    ty: TetraMCInstanceType,
    mod_actions: &[ModrinthProjectAction],
) -> Result<TetraMCInstance, String> {
    let mut minecraft_version_info_response =
        vanilla::client::retrieve_minecraft_version_data(&client, &url).await?;
    let client_path = vanilla::client::download_client(
        &app,
        &client,
        &minecraft_version_info_response,
        instance_name.as_ref(),
    )
    .await?;
    let (is_virtual, map_to_resources) = vanilla::assets::download_assets(
        &app,
        &client,
        &minecraft_version_info_response,
        &instance_name,
    )
    .await?;

    let natives_path = vanilla::libraries::download_libraries(
        &app,
        &client,
        &mut minecraft_version_info_response,
        &instance_name.as_ref(),
    )
    .await?;

    let mut libraries =
        vanilla::libraries::get_list_of_libraries(&minecraft_version_info_response)?;

    // Set minimum java version to java 8.
    let java_path = java::get_java_path(&app, &minecraft_version_info_response)?;

    let instance = match ty {
        TetraMCInstanceType::Fabric => {
            let loader_version = fabric::loader::get_latest_loader_version(&client).await?;
            let fabric_profile = fabric::loader::retrieve_fabric_profile(
                &client,
                &minecraft_version_info_response.id,
                &loader_version.version, //TODO: Make sure this is fine to be the absolute latest version of the loader, even for old minecraft versions
            )
            .await?;

            fabric::loader::download_libraries(&app, &client, &fabric_profile).await?;
            let mut fabric_libraries = fabric::loader::get_list_of_libraries(&fabric_profile)?;

            // FIXME: v This is ugllyyyyyyyy v
            let _ = fabric_profile
                .libraries
                .iter()
                .flat_map(|flib| flib.name.rsplit_once(":"))
                .flat_map(|(name, _)| {
                    if let Some(index) = libraries.iter().position(|lib| {
                        let (lib_name, _) = lib.name.rsplit_once(":").unwrap();
                        lib_name == name
                    }) {
                        Some(libraries.swap_remove(index))
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>();

            let mut libraries = libraries
                .iter()
                .map(|lib| lib.downloads.artifact.clone().unwrap().path)
                .collect::<Vec<_>>();

            libraries.append(&mut fabric_libraries);
            // FIXME: ^ This is ugllyyyyyyyy ^

            TetraMCInstance {
                name: instance_name.as_ref().to_owned(),
                created: Utc::now(),
                last_played: None,
                arguments: minecraft_version_info_response.arguments.clone(),
                fabric_arguments: Some(fabric_profile.arguments.clone()),
                main_class: fabric_profile.main_class.to_owned(),
                id: minecraft_version_info_response.id.to_owned(),
                assets: minecraft_version_info_response.assets.to_owned(),
                asset_index: minecraft_version_info_response.asset_index.clone(),
                ty,
                client_path: client_path
                    .to_str()
                    .ok_or("Failed to retrieve client path")?
                    .to_string(),
                virtual_assets: is_virtual,
                map_to_resources,
                java_path,
                natives_path,
                mods: Some(vec![]),
                ram_min: None,
                ram_max: None,
                client_log_config: None,
                libraries,
            }
        }
        TetraMCInstanceType::Vanilla => TetraMCInstance {
            name: instance_name.as_ref().to_owned(),
            created: Utc::now(),
            last_played: None,
            arguments: minecraft_version_info_response.arguments.clone(),
            fabric_arguments: None,
            main_class: minecraft_version_info_response.main_class.to_owned(),
            id: minecraft_version_info_response.id.to_owned(),
            assets: minecraft_version_info_response.assets.to_owned(),
            asset_index: minecraft_version_info_response.asset_index.clone(),
            ty,
            client_path: client_path
                .to_str()
                .ok_or("Failed to retrieve client path")?
                .to_string(),
            virtual_assets: is_virtual,
            map_to_resources,
            java_path,
            mods: None,
            natives_path,
            ram_min: None,
            ram_max: None,
            client_log_config: None,
            libraries: libraries
                .iter()
                .map(|lib| lib.downloads.artifact.clone().unwrap().path)
                .collect(),
        },
    };

    write_instance_toml(&app, &instance)?;
    Ok(instance)
}
