use tauri::Emitter;

use crate::{
    file::{self},
    models::DownloadStatusPayload,
    path::tetramc_libraries_dir,
};

// hehe, super models.
use super::models::{FabricLoaderVersion, FabricProfile};

const FABRIC_LOADER_VERSION_URL: &'static str = "https://meta.fabricmc.net/v2/versions/loader";
pub(crate) async fn get_latest_loader_version(
    client: &tauri::State<'_, reqwest::Client>,
) -> Result<FabricLoaderVersion, String> {
    Ok(client
        .get(FABRIC_LOADER_VERSION_URL)
        .send()
        .await
        .map_err(|e| format!("Failed to retrieve latest fabric loader version: {:?}", e))?
        .json::<Vec<FabricLoaderVersion>>()
        .await
        .map_err(|e| format!("Failed to convert fabric loader version to json: {:?}", e))?[0]
        .clone())
}

pub(crate) async fn retrieve_fabric_profile(
    client: &tauri::State<'_, reqwest::Client>,
    minecraft_version: impl AsRef<str>,
    loader_version: impl AsRef<str>,
) -> Result<FabricProfile, String> {
    Ok(client
        .get(format!(
            "https://meta.fabricmc.net/v2/versions/loader/{}/{}/profile/json",
            minecraft_version.as_ref(),
            loader_version.as_ref()
        ))
        .send()
        .await
        .map_err(|e| format!("Failed to retrieve fabric loader profile: {:?}", e))?
        .json::<FabricProfile>()
        .await
        .map_err(|e| format!("Failed to retrieve fabric loader profile: {:?}", e))?)
}

pub(crate) async fn download_libraries(
    app: &tauri::AppHandle,
    client: &tauri::State<'_, reqwest::Client>,
    fabric_profile: &FabricProfile,
) -> Result<(), String> {
    let libraries_dir = tetramc_libraries_dir(&app)?;

    let total_count = fabric_profile.libraries.iter().count();
    for (i, library) in fabric_profile.libraries.iter().enumerate() {
        // SAFETY: There should always be a name in this format.
        let (first, second) = library.name.split_once(":").unwrap();
        let mut file_path_server =
            format!("{}/{}/", first.replace(".", "/"), second.replace(":", "/"));

        let mut libraries_dir = libraries_dir.join(&file_path_server);

        if !libraries_dir.exists() {
            std::fs::create_dir_all(&libraries_dir)
                .map_err(|e| format!("Failed to create fabric library directory: {:?}", e))?;
        }

        let mut file_name = library
            .name
            .split(":")
            .skip(1)
            .collect::<Vec<_>>()
            .join("-");

        file_name.push_str(".jar");

        libraries_dir.push(&file_name);

        file_path_server.push_str(&file_name);

        let url = format!("{}{}", library.url, file_path_server);

        if !libraries_dir.exists() {
            app.emit(
                "download-instance-status",
                DownloadStatusPayload {
                    message: format!("Downloading: {} of {} library files.", i + 1, total_count),
                    percent: (i as f64 / total_count as f64) * 100.0,
                },
            )
            .map_err(|e| format!("Failed to download library artifact: {:?}", e))?;

            file::download_file(client, url, libraries_dir)
                .await
                .map_err(|e| format!("Failed to download fabric library file: {:?}", e))?;
        }
    }
    Ok(())
}

pub(crate) fn get_list_of_libraries(fabric_profile: &FabricProfile) -> Result<Vec<String>, String> {
    Ok(fabric_profile
        .libraries
        .iter()
        .map(|lib| {
            let (first, second) = lib.name.split_once(":").unwrap();
            let mut file_path_server =
                format!("{}/{}/", first.replace(".", "/"), second.replace(":", "/"));

            let mut file_name = lib.name.split(":").skip(1).collect::<Vec<_>>().join("-");

            file_name.push_str(".jar");
            file_path_server.push_str(&file_name);
            file_path_server
        })
        .collect())
}
