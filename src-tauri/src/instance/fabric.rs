use self::models::FabricGameVersion;

pub(crate) mod loader;
pub(crate) mod models;

#[tauri::command]
pub async fn get_supported_fabric_versions(
    client: tauri::State<'_, reqwest::Client>,
) -> Result<Vec<FabricGameVersion>, String> {
    Ok(client
        .get("https://meta.fabricmc.net/v2/versions/game")
        .send()
        .await
        .map_err(|e| format!("Failed to connect to fabric.net: {:?}", e))?
        .json::<Vec<FabricGameVersion>>()
        .await
        .map_err(|e| format!("Failed to map fabric game versions json object: {:?}", e))?)
}
