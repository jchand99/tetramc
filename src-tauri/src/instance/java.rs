use std::{fs::File, path::Path, process::Command};

use flate2::read::GzDecoder;
use tar::Archive;

use crate::path::tetramc_java_install_dir;

use super::vanilla::models::MinecraftVersionInfoResponse;

// Linux Download URL
pub const LINUX_JAVA_LATEST_URL: &'static str =
    "https://cdn.azul.com/zulu/bin/zulu23.30.13-ca-jdk23.0.1-linux_x64.tar.gz";
pub const LINUX_JAVA_BIN: &'static str = "zulu23.30.13-ca-jdk23.0.1-linux_x64/bin/java";
pub const LINUX_JAVA_JAR: &'static str = "zulu23.30.13-ca-jdk23.0.1-linux_x64/bin/jar";

// Mac Download URL
pub const MACOS_JAVA_LATEST_URL: &'static str =
    "https://cdn.azul.com/zulu/bin/zulu23.30.13-ca-jdk23.0.1-macosx_x64.tar.gz";
pub const MACOS_JAVA_BIN: &'static str = "zulu23.30.13-ca-jdk23.0.1-macosx_x64/bin/java";
pub const MACOS_JAVA_JAR: &'static str = "zulu23.30.13-ca-jdk23.0.1-macosx_x64/bin/jar";

// Windows Download URL
pub const WINDOWS_JAVA_LATEST_URL: &'static str =
    "https://cdn.azul.com/zulu/bin/zulu23.30.13-ca-jdk23.0.1-win_x64.zip";
pub const WINDOWS_JAVA_BIN: &'static str = "zulu23.30.13-ca-jdk23.0.1-win_x64/bin/java";
pub const WINDOWS_JAVA_JAR: &'static str = "zulu23.30.13-ca-jdk23.0.1-win_x64/bin/jar";

// Linux Download URL
pub const LINUX_JAVA_8_URL: &'static str =
    "https://cdn.azul.com/zulu/bin/zulu8.66.0.15-ca-jdk8.0.352-linux_x64.tar.gz";
pub const LINUX_JAVA_8_BIN: &'static str = "zulu8.66.0.15-ca-jdk8.0.352-linux_x64/bin/java";
pub const LINUX_JAVA_8_JAR: &'static str = "zulu8.66.0.15-ca-jdk8.0.352-linux_x64/bin/jar";

// Mac Download URL
pub const MACOS_JAVA_8_URL: &'static str =
    "https://cdn.azul.com/zulu/bin/zulu8.66.0.15-ca-jdk8.0.352-macosx_x64.tar.gz";
pub const MACOS_JAVA_8_BIN: &'static str = "zulu8.66.0.15-ca-jdk8.0.352-macosx_x64/bin/java";
pub const MACOS_JAVA_8_JAR: &'static str = "zulu8.66.0.15-ca-jdk8.0.352-macosx_x64/bin/jar";

// Windows Download URL
pub const WINDOWS_JAVA_8_URL: &'static str =
    "https://cdn.azul.com/zulu/bin/zulu8.66.0.15-ca-jdk8.0.352-win_x64.zip";
pub const WINDOWS_JAVA_8_BIN: &'static str = "zulu8.66.0.15-ca-jdk8.0.352-win_x64/bin/java";
pub const WINDOWS_JAVA_8_JAR: &'static str = "zulu8.66.0.15-ca-jdk8.0.352-win_x64/bin/jar";

pub async fn download(app: &tauri::AppHandle, client: &reqwest::Client) -> Result<(), String> {
    let (latest, eight, extension) = if cfg!(target_os = "linux") {
        (LINUX_JAVA_LATEST_URL, LINUX_JAVA_8_URL, ".tar")
    } else if cfg!(target_os = "windows") {
        (WINDOWS_JAVA_LATEST_URL, WINDOWS_JAVA_8_URL, ".zip")
    } else if cfg!(target_os = "macos") {
        (MACOS_JAVA_LATEST_URL, MACOS_JAVA_8_URL, ".tar")
    } else {
        return Err("Unsupported operating system!".to_string());
    };

    let java_dir = tetramc_java_install_dir(&app, None)?;
    if !java_dir.exists() {
        std::fs::create_dir_all(&java_dir)
            .map_err(|e| format!("Failed to create java directory: {:?}", e))?;
    }

    let latest_compressed_file = latest.split("/").last().unwrap();
    let latest_dir = latest_compressed_file.split(extension).next().unwrap();

    let eight_compressed_file = eight.split("/").last().unwrap();
    let eight_dir = eight_compressed_file.split(extension).next().unwrap();

    let mut latest_java_dir = tetramc_java_install_dir(&app, Some(latest_dir))?;
    let mut eight_java_dir = tetramc_java_install_dir(&app, Some(eight_dir))?;

    if !latest_java_dir.exists() {
        latest_java_dir.pop();
        latest_java_dir.push(latest_compressed_file);

        crate::file::download_file(client, latest, &latest_java_dir).await?;

        let file = std::fs::File::open(&latest_java_dir).map_err(|e| {
            format!(
                "Failed to open compressed java file for decompression: {:?}",
                e
            )
        })?;

        latest_java_dir.pop();

        if cfg!(target_os = "linux") || cfg!(target_os = "macos") {
            decompress_unix(&file, &latest_java_dir)?;
        } else if cfg!(target_os = "windows") {
            decompress_windows(&file, &latest_java_dir)?;
        } else {
            return Err("Unsupported operating system!".to_string());
        }

        latest_java_dir.push(latest_compressed_file);

        std::fs::remove_file(&latest_java_dir)
            .map_err(|e| format!("Failed to remove compressed java file: {:?}", e))?;
    }

    if !eight_java_dir.exists() {
        eight_java_dir.pop();
        eight_java_dir.push(eight_compressed_file);

        crate::file::download_file(client, eight, &eight_java_dir).await?;

        let file = std::fs::File::open(&eight_java_dir).map_err(|e| {
            format!(
                "Failed to open compressed java file for decompression: {:?}",
                e
            )
        })?;

        eight_java_dir.pop();

        if cfg!(target_os = "linux") || cfg!(target_os = "macos") {
            decompress_unix(&file, &eight_java_dir)?;
        } else if cfg!(target_os = "windows") {
            decompress_windows(&file, &eight_java_dir)?;
        } else {
            return Err("Unsupported operating system!".to_string());
        }

        eight_java_dir.push(eight_compressed_file);

        std::fs::remove_file(&eight_java_dir)
            .map_err(|e| format!("Failed to remove compressed java file: {:?}", e))?;
    }

    Ok(())
}

pub fn get_java_path(
    app: &tauri::AppHandle,
    minecraft_version_info_response: &MinecraftVersionInfoResponse,
) -> Result<Option<String>, String> {
    if minecraft_version_info_response.java_version.major_version <= 8 {
        let java_os = if cfg!(target_os = "linux") {
            Some(LINUX_JAVA_8_BIN)
        } else if cfg!(target_os = "windows") {
            Some(WINDOWS_JAVA_8_BIN)
        } else if cfg!(target_os = "macos") {
            Some(MACOS_JAVA_8_BIN)
        } else {
            None
        };

        let java_install_path = tetramc_java_install_dir(&app, java_os)?;
        Ok(Some(
            java_install_path.to_str().unwrap_or_default().to_owned(),
        ))
    } else {
        Ok(None)
    }
}

fn decompress_unix(file: &File, target_dir: impl AsRef<Path>) -> Result<(), String> {
    let tar = GzDecoder::new(file);
    let mut archive = Archive::new(tar);

    archive
        .unpack(target_dir.as_ref())
        .map_err(|e| format!("Failed to untar unix java tar file: {:?}", e))
}

fn decompress_windows(file: &File, target_dir: impl AsRef<Path>) -> Result<(), String> {
    zip_extract::extract(file, target_dir.as_ref(), false)
        .map_err(|e| format!("Failed to extract windows java zip file: {:?}", e))
}

pub fn extract_jar(app: &tauri::AppHandle, path: impl AsRef<Path>) -> Result<(), String> {
    #[cfg(target_os = "linux")]
    let java_path = tetramc_java_install_dir(&app, Some(LINUX_JAVA_JAR))?;
    #[cfg(target_os = "windows")]
    let java_path = tetramc_java_install_dir(&app, Some(WINDOWS_JAVA_JAR))?;
    #[cfg(target_os = "macos")]
    let java_path = tetramc_java_install_dir(&app, Some(MACOS_JAVA_JAR))?;

    let cached_cwd = std::env::current_dir()
        .map_err(|e| format!("Failed to get current working directory: {:?}", e))?;

    let parent = path.as_ref().parent().unwrap_or(Path::new(""));

    std::env::set_current_dir(parent).map_err(|e| {
        format!(
            "Failed to set current working directory to `{:?}`: {:?}",
            &parent, e
        )
    })?;

    let _output = Command::new(&java_path)
        .args(["xf", path.as_ref().to_str().unwrap_or_default()])
        .output()
        .map_err(|e| format!("Failed to extract jar file: {:?}", e))?;

    std::env::set_current_dir(cached_cwd)
        .map_err(|e| format!("Failed to reset current working directory: {:?}", e))?;
    println!("{:?}", std::env::current_dir().unwrap());
    Ok(())
}
