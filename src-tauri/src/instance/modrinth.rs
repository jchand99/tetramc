use models::{
    ModrinthProject, ModrinthProjectAction, ModrinthProjectSearchResult, ModrinthProjectVersion,
};
use tauri::Emitter;

use crate::{models::DownloadStatusPayload, path::tetramc_instance_subdir};

use super::{
    management,
    vanilla::models::{AddOn, TetraMCInstance},
};

pub mod models;

const MODRINTH_MOD_SEARCH_URL: &'static str = "https://api.modrinth.com/v2/search";
const MODRINTH_MOD_PROJECTS_URL: &'static str = "https://api.modrinth.com/v2/projects";
const MODRINTH_MOD_PROJECT_URL: &'static str = "https://api.modrinth.com/v2/project";
const MODRINTH_MOD_VERSION_URL: &'static str = "https://api.modrinth.com/v2/version";
const MODRINTH_MOD_VERSIONS_URL: &'static str = "https://api.modrinth.com/v2/versions";

#[tauri::command]
pub async fn modrinth_search_mods(
    search_term: String,
    game_type: String,
    game_version: String,
    offset: String,
    client: tauri::State<'_, reqwest::Client>,
) -> Result<ModrinthProjectSearchResult, String> {
    let facets = format!(
        "[[\"categories:{}\"], [\"versions:{}\"], [\"project_type:mod\"]]",
        &game_type, &game_version
    );

    Ok(client
        .get(MODRINTH_MOD_SEARCH_URL)
        .query(&[
            ("query", search_term.as_str()),
            ("facets", &facets),
            ("limit", "50"),
            ("offset", &offset),
        ])
        .header(
            "User-Agent",
            "jchand99/tetramc/1.0.0 (tetramc.unsold189@passmail.net)",
        )
        .send()
        .await
        .map_err(|e| e.to_string())?
        .json::<ModrinthProjectSearchResult>()
        .await
        .map_err(|e| {
            format!(
                "Failed to map modrinth search result json to object: {:?}",
                e
            )
        })?)
}

#[tauri::command]
pub async fn get_modrinth_add_ons(
    client: tauri::State<'_, reqwest::Client>,
    add_ons: Vec<AddOn>,
) -> Result<Vec<ModrinthProject>, String> {
    let project_ids = add_ons
        .iter()
        .map(|a| a.project_id.as_ref())
        .collect::<Vec<&str>>();
    let version_ids = add_ons
        .iter()
        .flat_map(|a| a.version_id.as_ref())
        .map(|n| n.as_ref())
        .collect::<Vec<&str>>();

    Ok(client
        .get(MODRINTH_MOD_PROJECTS_URL)
        .query(&[("ids", format!("{:?}", &project_ids))])
        .header(
            "User-Agent",
            "jchand99/tetramc/1.0.0 (tetramc.unsold189@passmail.net)",
        )
        .send()
        .await
        .map_err(|e| e.to_string())?
        .json::<Vec<ModrinthProject>>()
        .await
        .map_err(|e| format!("Failed to map modrinth mod results to object: {:?}", e))?)
}

#[tauri::command]
pub async fn get_modrinth_mod(
    client: tauri::State<'_, reqwest::Client>,
    m: String,
) -> Result<ModrinthProject, String> {
    Ok(client
        .get(format!("{}/{}", MODRINTH_MOD_PROJECT_URL, &m))
        .header(
            "User-Agent",
            "jchand99/tetramc/1.0.0 (tetramc.unsold189@passmail.net)",
        )
        .send()
        .await
        .map_err(|e| e.to_string())?
        .json::<ModrinthProject>()
        .await
        .map_err(|e| format!("Failed to map modrinth mod results to object: {:?}", e))?)
}

#[tauri::command]
pub async fn get_mod_upgrade_downgrade_status(
    client: tauri::State<'_, reqwest::Client>,
    previous_game_version: String,
    new_game_version: String,
    mut add_ons: Vec<AddOn>,
) -> Result<Vec<ModrinthProjectAction>, String> {
    let mut actions = Vec::with_capacity(add_ons.len());

    add_ons.sort_unstable_by(|a, b| a.project_id.cmp(&b.project_id));

    let project_ids = add_ons
        .iter()
        .map(|m| m.project_id.as_str())
        .collect::<Vec<_>>();

    let mut mod_results = client
        .get(MODRINTH_MOD_PROJECTS_URL)
        .query(&[("ids", format!("{:?}", &project_ids))])
        .header(
            "User-Agent",
            "jchand99/tetramc/1.0.0 (tetramc.unsold189@passmail.net)",
        )
        .send()
        .await
        .map_err(|e| e.to_string())?
        .json::<Vec<ModrinthProject>>()
        .await
        .map_err(|e| format!("Failed to map modrinth mod results to object: {:?}", e))?;

    mod_results.sort_unstable_by(|a, b| a.id.cmp(&b.id));

    for (add_on, mod_result) in add_ons.iter().zip(mod_results.iter()) {
        let results = client
            .get(format!(
                "{}/{}/version",
                MODRINTH_MOD_PROJECT_URL, add_on.project_id
            ))
            .query(&[
                ("game_versions", format!("[\"{}\"]", &new_game_version)),
                ("loaders", String::from("[\"fabric\"]")),
            ])
            .header(
                "User-Agent",
                "jchand99/tetramc/1.0.0 (tetramc.unsold189@passmail.net)",
            )
            .send()
            .await
            .map_err(|e| e.to_string())?
            .json::<Vec<ModrinthProjectVersion>>()
            .await
            .map_err(|e| format!("Failed to map modrinth mod results to object: {:?}", e))?;

        match (results.get(0), previous_game_version < new_game_version) {
            (Some(m), _) if add_on.version_id.as_ref() == Some(&m.id) => {
                actions.push(ModrinthProjectAction::NoOp {
                    file_name: add_on.file_name.clone().unwrap_or_default(),
                    project_title: mod_result.title.clone(),
                    project_id: m.id.clone(),
                })
            }
            (Some(m), true) => actions.push(ModrinthProjectAction::Upgrade {
                file_name: add_on.file_name.clone().unwrap_or_default(),
                project_title: mod_result.title.clone(),
                project_id: m.project_id.clone(),
                from_version_name: add_on.version_name.clone().unwrap_or_default(),
                to_version_name: m.name.clone(),
                from_version_id: add_on.version_id.clone().unwrap_or_default(),
                to_version_id: m.id.clone(),
            }),
            (Some(m), false) => actions.push(ModrinthProjectAction::Downgrade {
                file_name: add_on.file_name.clone().unwrap_or_default(),
                project_title: mod_result.title.clone(),
                project_id: m.project_id.clone(),
                from_version_name: add_on.version_name.clone().unwrap_or_default(),
                to_version_name: m.name.clone(),
                from_version_id: add_on.version_id.clone().unwrap_or_default(),
                to_version_id: m.id.clone(),
            }),
            (None, _) => actions.push(ModrinthProjectAction::Delete {
                project_id: add_on.project_id.clone(),
                project_title: mod_result.title.clone(),
            }),
        }
    }

    Ok(actions)
}

pub(crate) async fn download_mods(
    app: &tauri::AppHandle,
    client: &tauri::State<'_, reqwest::Client>,
    instance_name: impl AsRef<str>,
    mod_version_ids: &[(&str, &str)],
) -> Result<Vec<AddOn>, String> {
    if mod_version_ids.is_empty() {
        return Ok(Vec::new());
    }

    let mut mods_dir = tetramc_instance_subdir(app, instance_name.as_ref(), ".minecraft")?;
    mods_dir.push("mods");

    if !mods_dir.exists() {
        std::fs::create_dir_all(&mods_dir).map_err(|e| {
            format!(
                "Failed to create mods directory for `{}` instance: {:?}",
                instance_name.as_ref(),
                e
            )
        })?;
    }

    // let mut urls = Vec::new();
    let mut mods = Vec::new();
    let total_count = mod_version_ids.len();
    for (i, (project_id, version_id)) in mod_version_ids.iter().enumerate() {
        println!("{:?} / {:?}", project_id, version_id);
        // Get latest version of the mod at the time of download.
        let result = get_mod_version(&client, &version_id).await?;
        println!("{:?}", result);
        // FIXME: Modify this later with option to choose download version.
        mods_dir.push(&result.files[0].filename);

        app.emit(
            "download-instance-status",
            DownloadStatusPayload {
                message: format!("Downloading: {} of {} mods.", i + 1, total_count),
                percent: (i as f64 / total_count as f64) * 100.0,
            },
        )
        .map_err(|e| format!("Failed to download library artifact: {:?}", e))?;

        crate::file::download_file(client, &result.files[0].url, &mods_dir).await?;
        mods.push(AddOn {
            project_id: project_id.to_string(),
            version_id: Some(version_id.to_string()),
            version_name: Some(result.name.clone()),
            file_name: Some(result.files[0].filename.clone()),
            platform: Some(String::from("Modrinth")),
        });
        mods_dir.pop();
    }

    Ok(mods)
}

#[tauri::command]
pub async fn get_modrinth_mod_version(
    client: tauri::State<'_, reqwest::Client>,
    version: String,
) -> Result<ModrinthProjectVersion, String> {
    get_mod_version(&client, version).await
}

async fn get_mod_version(
    client: &tauri::State<'_, reqwest::Client>,
    version: impl AsRef<str>,
) -> Result<ModrinthProjectVersion, String> {
    Ok(client
        .get(format!("{}/{}", MODRINTH_MOD_VERSION_URL, version.as_ref()))
        .header(
            "User-Agent",
            "jchand99/tetramc/1.0.0 (tetramc.unsold189@passmail.net)",
        )
        .send()
        .await
        .map_err(|e| format!("Failed to retrieve mod version: {:?}", e))?
        .json::<ModrinthProjectVersion>()
        .await
        .map_err(|e| format!("Failed to parse mod version json: {:?}", e))?)
}

#[tauri::command]
pub async fn get_latest_modrinth_mod_version(
    client: tauri::State<'_, reqwest::Client>,
    project_id: String,
    game_version: String,
) -> Result<ModrinthProjectVersion, String> {
    get_latest_mod_version(&client, &project_id, &game_version).await
}

async fn get_latest_mod_version(
    client: &tauri::State<'_, reqwest::Client>,
    project_id: impl AsRef<str>,
    game_version: impl AsRef<str>,
) -> Result<ModrinthProjectVersion, String> {
    // FIXME: Make this not fabric dependant
    let versions = client
        .get(format!(
            "{}/{}/version",
            MODRINTH_MOD_PROJECT_URL,
            project_id.as_ref()
        ))
        .query(&[
            ("loaders", "[\"fabric\"]"),
            ("game_versions", &format!("[\"{}\"]", game_version.as_ref())),
        ])
        .header(
            "User-Agent",
            "jchand99/tetramc/1.0.0 (tetramc.unsold189@passmail.net)",
        )
        .send()
        .await
        .map_err(|e| format!("Failed to retrieve modrinth mod version: {:?}", e))?
        .json::<Vec<ModrinthProjectVersion>>()
        .await
        .map_err(|e| {
            format!(
                "Failed to deserialize json for mod `{:?}` | GameVersion `{:?}`: {:?}",
                project_id.as_ref(),
                game_version.as_ref(),
                e
            )
        })?;

    if versions.is_empty() {
        Err(String::from("No versions found for mod."))
    } else {
        Ok(versions[0].clone())
    }
}

#[tauri::command]
pub async fn get_modrinth_mod_versions(
    client: tauri::State<'_, reqwest::Client>,
    project_id: String,
    loader: String,
    game_version: String,
) -> Result<Vec<ModrinthProjectVersion>, String> {
    Ok(client
        .get(format!(
            "{}/{}/version",
            MODRINTH_MOD_PROJECT_URL, &project_id
        ))
        .query(&[("game_versions", format!("[\"{}\"]", &game_version))])
        .query(&[("loaders", format!("[\"{}\"]", &loader))])
        .header(
            "User-Agent",
            "jchand99/tetramc/1.0.0 (tetramc.unsold189@passmail.net)",
        )
        .send()
        .await
        .map_err(|e| format!("Failed to retrieve mod versions: {:?}", e))?
        .json::<Vec<ModrinthProjectVersion>>()
        .await
        .map_err(|e| format!("Failed to parse mod versions json: {:?}", e))?
        .into_iter()
        .collect())
}

#[tauri::command]
pub async fn update_mod(
    app: tauri::AppHandle,
    client: tauri::State<'_, reqwest::Client>,
    mut instance: TetraMCInstance,
    project_id: String,
    loader: String,
    game_version: String,
) -> Result<Option<AddOn>, String> {
    let mut mods_path = tetramc_instance_subdir(&app, &instance.name, ".minecraft")?;
    mods_path.push("mods");

    let latest = get_latest_mod_version(&client, &project_id, &game_version).await?;
    mods_path.push(&latest.files[0].filename);

    app.emit(
        "download-instance-status",
        DownloadStatusPayload {
            message: format!("Downloading: {} of {} mods.", 1, 1),
            percent: 0.0,
        },
    )
    .map_err(|e| format!("Failed to download library artifact: {:?}", e))?;

    crate::file::download_file(&client, &latest.files[0].url, &mods_path).await?;
    mods_path.pop();

    if let Some(mods) = &mut instance.mods {
        if let Some(index) = mods.iter().position(|m| m.project_id == project_id) {
            let m = mods.remove(index);
            if let Some(file_name) = m.file_name {
                mods_path.push(&file_name);
                std::fs::remove_file(&mods_path)
                    .map_err(|e| format!("Failed to remove old mod file: {:?}", e))?;
            }
        }

        let m = AddOn {
            project_id,
            version_id: Some(latest.id),
            version_name: Some(latest.name),
            file_name: Some(latest.files[0].filename.clone()),
            platform: Some(String::from("Modrinth")),
        };
        mods.push(m.clone());
        management::write_instance_toml(&app, &instance)?;

        Ok(Some(m))
    } else {
        Ok(None)
    }
}
