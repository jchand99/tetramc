use crate::path::{tetramc_assets_dir, tetramc_config_dir, tetramc_java_install_dir};
use serde::{Deserialize, Serialize};

const CONFIG_FILENAME: &'static str = "tetramc_config.toml";

#[derive(Debug, Serialize, Deserialize)]
pub struct GlobalConfig {
    pub java_path: String,
    pub assets_path: String,
    pub min_mem: usize,
    pub max_mem: usize,
}

#[tauri::command]
pub fn write_global_config(app: tauri::AppHandle, config: GlobalConfig) -> Result<(), String> {
    let mut config_dir = tetramc_config_dir(&app)?;
    config_dir.push(CONFIG_FILENAME);

    let content = toml::to_string::<GlobalConfig>(&config).map_err(|e| {
        format!(
            "Failed to convert GlobalConfig object to TOML string: {:?}",
            e
        )
    })?;

    std::fs::write(&config_dir, &content)
        .map_err(|e| format!("Failed to write global config file: {:?}", e))?;

    Ok(())
}

#[tauri::command]
pub fn read_global_config(app: tauri::AppHandle) -> Result<GlobalConfig, String> {
    let mut config_dir = tetramc_config_dir(&app)?;
    config_dir.push(CONFIG_FILENAME);

    if config_dir.exists() {
        let contents = std::fs::read_to_string(&config_dir)
            .map_err(|e| format!("Failed to read global config TOML file: {:?}", e))?;

        Ok(toml::from_str::<GlobalConfig>(&contents).map_err(|e| {
            format!(
                "Failed to convert global config TOML string to GlobalConfig object: {:?}",
                e
            )
        })?)
    } else {
        let java_bin_path = if cfg!(target_os = "linux") {
            crate::instance::java::LINUX_JAVA_BIN
        } else if cfg!(target_os = "windows") {
            crate::instance::java::WINDOWS_JAVA_BIN
        } else if cfg!(target_os = "macos") {
            crate::instance::java::MACOS_JAVA_BIN
        } else {
            return Err("Unsupported operating system!".to_string());
        };

        let java_path = if let Ok(tetramc_java_downloads_path) =
            tetramc_java_install_dir(&app, Some(java_bin_path))
        {
            std::env::var("JAVA_PATH").ok().unwrap_or(
                tetramc_java_downloads_path
                    .to_str()
                    .unwrap_or("")
                    .to_string(),
            )
        } else {
            String::new()
        };

        let assets_path = if let Ok(assets_path) = tetramc_assets_dir(&app) {
            assets_path.to_str().unwrap_or("").to_string()
        } else {
            String::new()
        };

        Ok(GlobalConfig {
            java_path,
            assets_path,
            max_mem: 1024,
            min_mem: 1024,
        })
    }
}
