use std::{io::Cursor, path::Path};

use reqwest::StatusCode;

pub fn write_cache_file(
    app: &tauri::AppHandle,
    content: impl AsRef<str>,
    file_name: impl AsRef<str>,
) -> Result<(), String> {
    let mut cache_dir = crate::path::tetramc_cache_dir(&app)?;

    if !cache_dir.exists() {
        std::fs::create_dir_all(&cache_dir)
            .map_err(|e| format!("Unable to create cache directory: {:?}", e))?;
    }

    cache_dir.push(file_name.as_ref());

    std::fs::write(cache_dir, content.as_ref())
        .map_err(|e| format!("Unable to write cache file: {:?}", e))?;

    Ok(())
}

pub fn read_cache_file(
    app: &tauri::AppHandle,
    file_name: impl AsRef<str>,
) -> Result<String, String> {
    let mut cache_dir = crate::path::tetramc_cache_dir(&app)?;

    println!("Cache dir: {:?}", &cache_dir);
    if !cache_dir.exists() {
        std::fs::create_dir_all(&cache_dir)
            .map_err(|e| format!("Unable to create cache directory: {:?}", e))?;
    }

    cache_dir.push(file_name.as_ref());

    Ok(std::fs::read_to_string(cache_dir)
        .map_err(|e| format!("Unable to read cache file: {:?}", e))?)
}

pub async fn download_file(
    client: &reqwest::Client,
    url: impl AsRef<str>,
    dir: impl AsRef<Path>,
) -> Result<(), String> {
    match client.get(url.as_ref()).send().await {
        Ok(response) => {
            let status = response.status();
            match status {
                StatusCode::OK => {
                    let mut file = std::fs::File::create(dir.as_ref()).map_err(|e| {
                        format!(
                            "Failed to create file `{:?}` for download: {:?}",
                            dir.as_ref(),
                            e
                        )
                    })?;

                    let bytes = response
                        .bytes()
                        .await
                        .map_err(|e| format!("Failed to retrieve bytes to download: {:?}", e))?;

                    let mut content = Cursor::new(bytes);

                    std::io::copy(&mut content, &mut file)
                        .map_err(|e| format!("Failed to copy downloaded bytes to file: {:?}", e))?;

                    Ok(())
                }
                _ => Err(format!(
                    "Failed to download file with status code: {:?}",
                    status
                )),
            }
        }
        Err(e) => Err(format!("Failed to download file: {:?}", e)),
    }
}
