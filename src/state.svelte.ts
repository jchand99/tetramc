import type {
  FabricGameVersion,
  GlobalConfig,
  Message,
  MinecraftAccount,
  ModrinthProject,
  ModrinthProjectVersion,
  TetraMCInstance,
} from './types';

let darkModeState: boolean = $state(false);
export const darkMode = {
  get() {
    return darkModeState;
  },
  set(value: boolean) {
    darkModeState = value;
  },
};

interface IPageState {
  page: string;
}
export const pageState: IPageState = $state({
  page: '',
});

interface IAccountState {
  minecraftAccount: MinecraftAccount | null;
}
export const accountState: IAccountState = $state({
  minecraftAccount: null,
});

interface IMessageState {
  messages: Message[];
  messageIdCount: number;
}
export const messageState: IMessageState = $state({
  messages: [],
  messageIdCount: 0,
});

interface IGlobalConfigState {
  globalConfig: GlobalConfig | null;
}
export const globalConfigState: IGlobalConfigState = $state({
  globalConfig: null,
});

interface IInstanceState {
  runningInstances: string[];
  editingInstance: TetraMCInstance | null;
  supportedFabricVersions: FabricGameVersion[] | null;
}
export const instanceState: IInstanceState = $state({
  runningInstances: [],
  editingInstance: null,
  supportedFabricVersions: null,
});

interface IViewModState {
  viewModPromise: Promise<ModrinthProject | null> | null;
  viewModVersions: ModrinthProjectVersion[] | null;
}
export const viewModState: IViewModState = $state({
  viewModPromise: null,
  viewModVersions: null,
});

export const downloadingState: { state: true | false } = $state({
  state: false,
});
