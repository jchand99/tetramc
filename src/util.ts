import { invoke } from '@tauri-apps/api/core';
import { messageState } from './state.svelte';
import type { MessageType } from './types';

export function sendError(message: string) {
  sendMessage(message, 'Error');
}

export function sendWarning(message: string) {
  sendMessage(message, 'Warning');
}

export function sendInfo(message: string) {
  sendMessage(message, 'Info');
}

function sendMessage(message: string, type: MessageType) {
  console.log(message);
  messageState.messages.push({
    id: messageState.messageIdCount,
    ty: type,
    createdAt: Date.now(),
    message: `${message}`,
  });
  messageState.messageIdCount += 1;
}

export async function run<T>(
  command: string,
  params: any = null
): Promise<T | null> {
  try {
    let result = await invoke<T>(command, params);
    return <T>result;
  } catch (err) {
    sendError(err as string);
    return null;
  }
}
