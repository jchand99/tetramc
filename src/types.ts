export type MessageType = 'Error' | 'Warning' | 'Info';
export type Message = {
  id: number;
  ty: MessageType;
  createdAt: number;
  message: string;
};

// Azure
export type AzureAuthResponse = {
  user_code: string;
  device_code: string;
  verification_uri: string;
  expires_in: number;
  interval: number;
  message: string;
};

export type AzureTokenResponse = {
  token_type: string;
  scope: string;
  expires_in: number;
  ext_expires_in: number;
  access_token: string;
  refresh_token: string;
};

// Minecraft
export type MinceraftAuthMetadata = {};

export type MinecraftAuthResponse = {
  username: string;
  roles: string[];
  //metadata: MinceraftAuthMetadata,
  access_token: string;
  expires_in: number;
  token_type: string;
};

export type MinecraftItem = {
  name: string;
  signature: string;
};

export type MinecraftOwnershipResponse = {
  items: Array<MinecraftItem>;
  signature: string;
  keyId: string;
};

export type MinecraftSkin = {
  id: string;
  state: string;
  url: string;
  variant: string;
};

export type MinecraftCape = {
  id: string;
  state: string;
  url: string;
  alias: string;
};

export type MinecraftProfile = {
  id: string;
  name: string;
  skins: Array<MinecraftSkin>;
  capes: Array<MinecraftCape>;
};

export type MinecraftAccount = {
  profile: MinecraftProfile;
  auth: MinecraftAuthResponse;
  azure_token: AzureTokenResponse;
  azure_auth_response: AzureAuthResponse;
};

export type MinecraftVersionLatest = {
  release: string;
  snapshot: string;
};

export type MinecraftVersion = {
  id: string;
  ty: string;
  url: string;
  time: string;
  release_time: string;
};

export type MinecraftVersionResponse = {
  latest: MinecraftVersionLatest;
  versions: MinecraftVersion[];
};

// Account
export type Skin = {
  id: string;
  state: string;
  url: string;
  variant: string;
};

export type Cape = {
  id: string;
  state: string;
  url: string;
  alias: string;
};

export type ProfileAction = {};

export type AccountUpdateResponse = {
  id: string;
  name: string;
  skins: Array<Skin>;
  capes: Array<Cape>;
  profile_actions?: ProfileAction;
};

export type UsernameStatusResponse = {
  changed_at: string;
  created_at: string;
  name_change_allowed: boolean;
};

export type NameAvailabilityResposne = {
  status: string;
};

// Minecraft Instance Models
export type MinecraftInstanceAssetIndex = {
  id: string;
  sha1: string;
  size: number;
  total_size: number;
  url: string;
};

export type MinecraftInstanceDownloadsClient = {
  sha1: string;
  size: number;
  url: string;
};

export type MinecraftInstanceDownloads = {
  client: MinecraftInstanceDownloadsClient;
  client_mappings?: MinecraftInstanceDownloadsClient;
  server: MinecraftInstanceDownloadsClient;
  server_mappings?: MinecraftInstanceDownloadsClient;
};

export type MinecraftInstanceJavaVersion = {
  component: string;
  major_version: number;
};

export type MinecraftInstanceLibraryDownloadsArtifact = {
  path: string;
  sha1: string;
  size: number;
  url: string;
};

export type MinecraftInstanceLibraryClassifiers = {
  natives_linux?: MinecraftInstanceLibraryDownloadsArtifact;
  natives_osx?: MinecraftInstanceLibraryDownloadsArtifact;
  natives_windows?: MinecraftInstanceLibraryDownloadsArtifact;
  javadoc?: MinecraftInstanceLibraryDownloadsArtifact;
  sources?: MinecraftInstanceLibraryDownloadsArtifact;
};

export type MinecraftInstanceLibraryDownloads = {
  artifact?: MinecraftInstanceLibraryDownloadsArtifact;
  classifiers?: MinecraftInstanceLibraryClassifiers;
};

export type MinecraftInstanceLibraryRuleOs = {
  name: string;
};

export type MinecraftInstanceLibraryExtract = {
  exclude: Array<string>;
};

export type MinecraftInstanceLibraryNatives = {
  linux?: string;
  windows?: string;
  osx?: string;
};

export type MinecraftInstanceLibrary = {
  downloads: MinecraftInstanceLibraryDownloads;
  extract?: MinecraftInstanceLibraryExtract;
  natives?: MinecraftInstanceLibraryNatives;
  name: string;
  rules?: Array<MinecraftInstanceRule>;
};

export type MinecraftInstanceLoggingFile = {
  id: string;
  sha1: string;
  size: number;
  url: string;
};

export type MinecraftInstanceLoggingClient = {
  argument: string;
  file: MinecraftInstanceLoggingFile;
  ty: string;
};

export type MinecraftInstanceLogging = {
  client: MinecraftInstanceLoggingClient;
};

export type MinecraftInstanceRuleOs = {
  name?: string;
  version?: string;
  arch?: string;
};

export type MinecraftInstanceRuleFeature = {
  is_demo_user?: boolean;
  has_custom_resolution?: boolean;
};

export type MinecraftInstanceRule = {
  action?: string;
  os?: MinecraftInstanceRuleOs;
  features?: MinecraftInstanceRuleFeature;
};

export type MinecraftInstanceArgument = {
  rules: Array<MinecraftInstanceRule | string>;
  value: Array<string> | string;
};

export type MinecraftInstanceArgumentsEnum = {
  args: MinecraftInstanceArguments | string;
};

export type MinecraftInstanceArguments = {
  game: Array<MinecraftInstanceArgument>;
  jvm: Array<MinecraftInstanceArgument>;
};

export type MinecraftVersionInfoResponse = {
  arguments: MinecraftInstanceArgumentsEnum;
  asset_index: MinecraftInstanceAssetIndex;
  assets: string;
  compliance_level: number;
  downloads: MinecraftInstanceDownloads;
  id: string;
  java_version: MinecraftInstanceJavaVersion;
  libraries: Array<MinecraftInstanceLibrary>;
  logging?: MinecraftInstanceLogging;
  main_class: string;
  minimum_launcher_version: number;
  release_time: string;
  time: string;
  ty: string;
};

export enum TetraMCInstanceType {
  Vanilla = 'Vanilla',
  Fabric = 'Fabric',
}

export type AddOn = {
  project_id: string;
  version_id?: string;
  version_name?: string;
  file_name?: string;
  platform?: string;
};

export type TetraMCInstance = {
  name: string;
  created: Date;
  last_played?: Date;
  arguments: MinecraftInstanceArguments;
  main_class: string;
  id: string;
  assets: string;
  asset_index: MinecraftInstanceAssetIndex;
  ty: TetraMCInstanceType;
  client_path: string;
  virtual_assets: boolean;
  mods?: AddOn[];
  map_to_resources: boolean;
  java_path?: string;
  natives_path?: string;
  ram_min?: number;
  ram_max?: number;
  libraries: string[];
};

export type GlobalConfig = {
  java_path: string;
  assets_path: string;
  min_mem: number;
  max_mem: number;
};

export type ModrinthProjectSearchResult = {
  hits: ModrinthProjectHit[];
  offset: number;
  limit: number;
  total_hits: number;
};

export type ModrinthProjectHit = {
  project_id: string;
  project_type: string;
  slug: string;
  author: string;
  title: string;
  description: string;
  categories: string[];
  display_categories: string[];
  versions: string[];
  downloads: number;
  follows: number;
  icon_url: string;
  date_created: Date;
  date_modified: Date;
  latest_version: string;
  client_side: string;
  server_side: string;
  gallery: string[];
  featured_gallery?: string[];
  color?: number;
};

export type ModrinthProject = {
  client_side: string;
  server_side: string;
  game_versions: string[];
  id: string;
  slug: string;
  project_type: string;
  team: string;
  organization?: string;
  title: string;
  description: string;
  body: string;
  body_url?: string;
  published: string;
  updated: string;
  approved?: string;
  queued?: string;
  status: string;
  requested_status: string;
  moderator_message: ModrinthProjectMessage;
  license: ModrinthProjectLicense;
  downloads: number;
  followers: number;
  categories: string[];
  additional_categories: string[];
  loaders: string[];
  versions: string[];
  icon_url?: string;
  issues_url: string;
  source_url: string;
  wiki_url: string;
  discord_url: string;
  donation_urls: ModrinthProjectDonationUrl[];
  gallery: ModrinthProjectGallery[];
  color: number;
  thread_id: string;
  monetization_status: string;
};
export type ModrinthProjectVersion = {
  game_versions: string[];
  loaders: string[];
  id: string;
  project_id: string;
  author_id: string;
  featured: boolean;
  name: string;
  version_number: string;
  changelog: string;
  changelog_url?: string;
  date_published: string;
  downloads: number;
  version_type: string;
  status: string;
  requested_status?: string;
  files: ModrinthProjectFile[];
  dependencies: any;
};

export type ModrinthProjectFile = {
  hashes: ModrinthProjectFileHashes;
  url: string;
  filename: string;
  primary: boolean;
  size: number;
  file_type?: string;
};

export type ModrinthProjectFileHashes = {
  sha512: string;
  sha1: string;
};

export type ModrinthProjectDonationUrl = {
  id: string;
  platform: string;
  url: string;
};

export type ModrinthProjectGallery = {
  url: string;
  raw_url: string;
  featured: boolean;
  title?: string;
  description?: string;
  created: string;
  ordering: number;
};

export type ModrinthProjectLicense = {
  id: string;
  name: string;
  url: string;
};

export type ModrinthProjectMessage = {
  message: string;
  body?: string;
};

// Fabric
export type FabricProfile = {
  id: string;
  inherits_from: string;
  release_time: string;
  time: string;
  ty: string;
  main_class: string;
  arguments: FabricArguments;
  libraries: Array<FabricLibrary>;
};

export type FabricArguments = {
  game: Array<String>;
  jvm: Array<String>;
};

export type FabricLibrary = {
  name: string;
  url: string;
  md5?: string;
  sha1?: string;
  sha256?: string;
  sha512?: string;
  size?: number;
};

export type FabricGameVersion = {
  version: String;
  stable: boolean;
};

// Other
export type DownloadStatusPayload = {
  message: string;
  percent: number;
};
