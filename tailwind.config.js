/** @type {import('tailwindcss').Config} */
export default {
  mode: 'jit',
  content: ['./src/**/*.{html,svelte,js,ts}'],
  darkMode: 'selector',
  theme: {
    screens: {
      hhd: '960px',
      // -> @media (min-width: 960px) { ... }
      hd: '1920px',
      // -> @media (min-width: 1920px) { ... }
      qhd: '2560px',
      // -> @media (min-width: 2560px) { ... }
      uhd: '3840px',
      // -> @media (min-width: 3840px) { ... }
      wide: '7680px',
      // -> @media (min-width: 7860px) { ... }
    },
    extend: {},
  },
  plugins: [],
};
